# Dependency inversion principle (DIP)

High level module should not depend on low level modules. Both should depend on abstractions. Abstraction should not depend upon details. Details should depend on abstraction.

Have you ever fould yourself standing in a shoe store wondering if you should get the brown or the black pair, only to get home and regret your choice? Sadly, you've bought them, they're yours. Programing against concrete implementations is the same thing: once you choose, you are stuch with it, refunds and refactoring. But why choose when you dont' have to?

The Dependency Inversion Principle (DIP) tells us that the most flexible systems are those in which source code dependencies refer only to abstractions, not to concretions.

```go
// package person
type Person struct {
    shoe IShoe
}

// package shoe
type IShoe interface {
    Color()
}

type BrownShoe struct {

}

func (B BrownShoe) Color() {}
```

The Shoes package owns the Shoe interface, which is entirely logical. However, problems arise when the requirements change. Changes to the Shoes package are likely to cause the Shoe interface to want to change. This will, require the Person object to change. Any new features that we add to the Shoe interface may be not be needed or relevant to the Person object. Therefore, the Person object is still coupled to the Shoe package.

There are two key points here. Firstly, the DIP forces us to focusd on the ownership of the abstraction. In our example, that means moving the interface into the package where it was used and changing the relationship from uses to requires, it's a subtly difference, but an important one.

Secondly, the DIP encourages us to decouple usage requirements from implementations. In our example, our Brown Shoes object implements Footware, but it's not hard to imagine a lot more implemenations and some might not even be shoes.

```go
// package people
type person struct {
    i footware
}

type footware interface {
    Color()
}

// package shoe
type brownshoes struct {

}

func (s brownshoes) Color() {}
```

## Stable abstractions

Every change to an abstract interface corresponds to a change to its concrete implementations. Conversely, changes to concrete implementations do not always, or even usually require changes to the interfaces that they implement. Therefore interfaces are less volatile than implementations.

**Don't override concrete functions.** Concrete functions often require source code depdendencies. When you override those functions, you do not eliminate those dependencies - indeed, you inherit them. To manage those dependencies, you should make the function abstract and create multiple implementations

## How does this relate to DI?

Dependency inversion is very easy to mistake for dependency injection, and many, including me for a long time, assume that they are equivalent. But as we have seen dependency inversion focused on the ownership of the dependencies's abstractrion definition, and DI is focused on using those abstractions.

By applying DIP with DI, we end up with very well decoupled packages that are incredibly easy to understand easy to extend, and simple to test.

## What does this mean for Go?

We have talked before about Go's support for implicit interfaces and how we can leverage that to define our dependencies as interfaces in the same package, rather than importing an interface from another package. This approach is DIP.

Perhaps your inner skeptic is going crazy, yelling, but this would mean I would have to define interfaces everywhere! Yes, that might be true. It could event result in a small amount of duplication. You will find however, that the interfaces you would have define without dependency inversion would have been fatter and more unwieldy, a fact that would have cost you more to work with in the future.

After applying DIP, you are unlikely to have any circular dependency issues. In fact, you will almost certainly find that the number of imports in your code drops significantly and your depdendency graph decomes rather flat. In fact, many packages will only be imported by the main package.
