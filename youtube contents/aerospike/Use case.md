# Top Aerospike NoSQL Use Cases

Aerospike is a row-oriented database. It's a client-server solution, which allows you to build an app server cluster that attaches over TCP to your database cluster using Aerospike's provided libraries.

|||
|---|---|
|Key-Value Database|Key-value is predictable, and in-memory systems like Redis are very fast|
|Cache Replacement|low latancy and high throughput make it an excellent cache replacement|
|User profile|advertising or marketing application, you'll need to store user profiles|
|recommendation Engine||
