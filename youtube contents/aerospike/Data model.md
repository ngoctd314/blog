# Aerospike data model

## Components of the Aerospike schemaless data model

|Component|Description|
|---|---|
|physical storage|You can choose the specific type of storage you want for each namespace: NVMe Flash, DRAM, or PMEM. Diff ns in the same cluster can use distinct types of storage|
|namespace|a ns is a collection of records that share one specific storage engine, with common policies such as replication factor, encryption and more. A database can contain multiple namespaces|
|record|A record is a contiguous storage unit for all the data uniquely identified by a single key|
|set|Records can be optionally grouped into sets|
|bin|A record is subdivied into bins. Each bin has its own data type, and those do not need to agree between records. Secondary indexes can optionally be declared on bins|
