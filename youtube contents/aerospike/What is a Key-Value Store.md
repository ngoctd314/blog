# What is a Key-Value Store?

A key-value store or key-value database that uses map or dictionary as the fundamental data model where each key is associated with one and only one value in a collection.

Key-value stores have no queue languages. They provide a way to store retrieve and update data using simple get, put and delete commands

## Scalability and Reliability

Key-value stores scale out by implementing partitioning (storing data on more than one node), replication and auto recovery.

## Use Cases and Implementations

Key-value stores handle size well and are good at processing a constant stream of read/write operations with low latency making them perfect for:

- Session management at high scale
- User perference and profile stores
- Product recommendations
- Ad servicing
- Can effectively work as a cache for heavily accessed but rarely updated data
