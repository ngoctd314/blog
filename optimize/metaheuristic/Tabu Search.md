# Tabu Search

Applying one of the most popular metaheuristics algorithms called Tabu Search to solve a NP-hard scheduling problem using Python. A metaheuristic is a technique designed to solve problems with an approximate solution when classic methods fail to find an exact solution in a reasonable amount of time (NP-hard problems).

Tabu Search is one of these metaheuristic techniques and it's one of the famous ones due to its capacibility to efficiently solve a variety of problems.

In this article, we will explore and get to know how does TS works through applying it to solve The Single machine total weighted tardiness problem which is an NP-hard problem.

## The Single Machine Total Weighted Tardiness Problem

We have a single machine that can handle only one job at a time, and there is an N number of jobs (or tasks) to be processed without interruption on the machine. Each job i ∈ N requires an integer processing time Pi. And has a positive weight Wi indicates the importance of the job and a due date di. If we assume that the machine becomes avaiable for processing at time zero, we can indicate the completion time of job i as Ci and the the tardiness of the job can be calculated as Ti = max{Ci-di, 0}, so if the job is processed before its due data Ci <= di, the will be no tardiness T = 0.

The objectives is to order the N jobs in a way that minimizes the total weighted tardiness of the whole process, i.e min ∑WiTi, notive that if job has a higher weight the penalty of tardiness will be higher.

## Tabu Search Basic Algorithm

TS was first proposed by Glover in 1986 and was also developed by Hansen in parallel. In a nutshell, TS tries to find the best admissible solution in the neighborhood of the current solution in each iteration, considering recent solutions as "Tabu" to prevent cycling.

### Step 0

The initial step is to create an initial solution so the algorithm can iterate over it and find a better one. The initial solution can be seen as the starting point of the algorithm, in most cases, this initial solution is assigned randomly, however, if you have a better understanding of the problem you could design a specific algorithm to construct the initial solution.

### Step 1

Now that we have the initial solution, the next step is to create the list of candidate solution from the current solution S (initial solution in iteration 0), we call these solutions neighbors or the neighborhood of S. To find the neighbor solutions from the current solution S, we need to define what is called a neighborhood function, under this function each solution S has an associated subset of solutions.

### Step 2

From the neighborhood solutions list created in step 1, we choose best admissible (Non-tabu or meets aspiration criteria) solution by checking each solution.

### Concept make TS powerful and unique

**Tabu List**

This is the list TS uses to record the recent solutions and prevents them to reoccur for a specified number of iterations, which helps the search to move away from previously visited solutions thus performs more extensive exploration.

- Tabu Tenure: This defines the size of the Tabu list, i.e for how many iterations a solution component is kept as Tabu. Notice that Tabu Tenure has a great impact on the TS performance and we can say that the smaller the tenure, the higher the probability of cycling. In general, there is no simple rule of setting the Tabu Tenure, it highly depends on the problem you are trying to solve.

- Tabu Attribute: As you might have noticed, if we have big Tabu tenure and big problem instances like 10000 number of jobs in our problem instead of 10, recording tall visited solutions in the tabu list will be very consuming and expensive. Instead, we store the moves performed rather than the whole solution. Tabu Attributes defines the solution component kept in the tabu list. From the example described in Tabu Tenure above, we could store the jobs that have been swapped, Tabu list = {(8, 3), (7, 9)}, which means, do not swap jobs (8,3) or (7,9) for the next two iterations.

**Aspiration Criteira**

### Step 3

Check the defined stopping criteria, this can be the maxnumber of iterations reached or th:qe time of running, if the stopping criteria are not met, go to step 4, if the stopping criteria are met terminate and return the best solution.

### Step 4

Update Tabu list, Aspiration Criteria and go to Step 1

Tabu search has 3 main strategies:

Forbidding strategy: controls what enters the tabu list

Freeing strategy: controls what exits the tabu list

Short-term strategy: manages interplay between the forbidding strategy and freeing strategy to select trial solutions
