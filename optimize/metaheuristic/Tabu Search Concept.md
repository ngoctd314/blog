# Tabu Search Concept

Tab search is a metaheuritic search method employing local search methods used for mathenmatical optimization.

Local (neighborhood) searches take a potential solution to a problem and check its immediate neighbors in the hope of finding an improved solution.

Tabu search enhances the performance of local search by relaxing its basic rule. The implementation of tabu search uses memory structures that describe the visited solutions or user-provided sets of rules. If a potential solution has been previously visited within a certain short-term period or if it has violated a rule, it is marked as "tabu" (forbidden) so that the algorithm does not consider that possibility repeatedly.

## Basic description

Tabu search uses a local or neighborhood search procedure to iteratively move from one potential solution x to an improved solution x' in the neighborhood of x, until some stopping criterion has been satisfied (generall, an attempt limit or a score threshold). Local search procedures often become stuck in poor-scoring areas of areas where scores plateau.
