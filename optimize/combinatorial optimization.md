# Combinatorial Optimization

Combinatorial optimization consists of finding an optimal object from a finite set of objects, where the set of feasible solutions is discrete or can be reduced to discrete set. Exhaustive search is not tratable, and so specialized algorithms that quickly rule out range parts of the search space or approximation algorithms must be resorted to instead.
