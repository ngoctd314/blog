# Description

In this example, we are going to use OpenTelemetry and Jaeger to trace application request flow.

## Flow

1. User sends a HTTP request

2. Controller object catches it

3. Request object validates it

4. Service object handles it

5. Storage object stores it

6. Controller responds to it

Each span in each steps are closed/ended once their job is done.
