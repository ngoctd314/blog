package trace

import (
	"context"

	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/codes"
	"go.opentelemetry.io/otel/trace"
)

// NewSpan .
func NewSpan(ctx context.Context, name string, cus SpanCustomiser) (context.Context, trace.Span) {
	if cus == nil {
		return otel.Tracer("").Start(ctx, name)
	}

	return otel.Tracer("").Start(ctx, name, cus.customise())
}

// SpanFromContext returns the current span from a context. If you wish to avoid
// creating child spans for each operation and just rely on the parent span, use
// this function throughout the application. With such practice you will get
// flatter span tree as opposed to deeper version.
func SpanFromContext(ctx context.Context) trace.Span {
	return trace.SpanFromContext(ctx)
}

// AddSpanTags adds new tags to the span. It will appear under "Tags" section of the selected span.
// This tags if you think the tag and its value could be useful while debugging
func AddSpanTags(span trace.Span, tags map[string]string) {
	list := make([]attribute.KeyValue, len(tags))

	var i int
	for k, v := range tags {
		list[i] = attribute.Key(k).String(v)
		i++
	}

	span.SetAttributes(list...)
}

// AddSpanEvents adds a new events to the span. It will appear under the "Logs" section of the selected span.
// Use this if the event could mean anything valuable while debugging.
func AddSpanEvents(span trace.Span, name string, events map[string]string) {
	list := make([]trace.EventOption, len(events))
	var i int
	for k, v := range events {
		list[i] = trace.WithAttributes(attribute.Key(k).String(v))
		i++
	}

	span.AddEvent(name, list)
}

// AddSpanError adds a new event to the span. It will appear under the "Logs" section of selected span. This is not
// going to flag the span as "failed". Use this if you think you should log any exception such as critical, error
// warning, caution etc. Avoid logging sensitive data!
func AddSpanError(span trace.Span, err error) {
	span.RecordError(err)
}

func FailSpan(span trace.Span, msg string) {
	span.SetStatus(codes.Error, msg)
}

// SpanCustomiser is used to enforce custom span options. Any
type SpanCustomiser interface {
	customise() []trace.SpanOption
}
