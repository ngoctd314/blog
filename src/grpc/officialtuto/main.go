package main

import (
	"io/ioutil"
	"log"
	"officialtuto/pb"

	"google.golang.org/protobuf/proto"
	"google.golang.org/protobuf/types/known/timestamppb"
)

func main() {
	p1 := &pb.Person{
		Name:        "TDn",
		Id:          1,
		Email:       "tdn@gmail.com",
		Phones:      []*pb.Person_PhoneNumber{},
		LastUpdated: &timestamppb.Timestamp{},
	}
	book := &pb.AddressBook{
		People: []*pb.Person{p1},
	}

	// Write the new address book back to disk
	out, err := proto.Marshal(book)
	if err != nil {
		log.Fatalln("Failed to encode address book:", err)
	}
	if err := ioutil.WriteFile("test.json", out, 0644); err != nil {
		log.Fatal("Failed to write address book: ", err)
	}
}
