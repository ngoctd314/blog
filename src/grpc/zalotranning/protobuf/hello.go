package main

import (
	"context"

	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
)

type HelloServiceImpl struct{}

func (p *HelloServiceImpl) Hello(ctx context.Context, args *String) (*String, error) {
	reply := &String{
		state:         protoimpl.MessageState{},
		sizeCache:     0,
		unknownFields: []byte{},
		Value:         "",
	}
}
