package main

import (
	"log"
	"net"
	"net/rpc"
)

type HelloWorldService struct{}

func (s *HelloWorldService) Greeting(author string, greeting *string) error {
	*greeting = "Hello world " + author
	return nil
}

func main() {
	rpc.RegisterName("HelloWorldService", new(HelloWorldService))
	// run rpc server on port 1234
	listener, err := net.Listen("tcp", ":1234")
	if err != nil {
		log.Fatal("ListenTCP error: ", err)
	}

	for {
		conn, err := listener.Accept()
		if err != nil {
			log.Fatal("Accept error: ", err)
		}

		go rpc.ServeConn(conn)
	}
}
