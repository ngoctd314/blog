package main

import (
	"fmt"
	"log"
	"net/rpc"
)

func main() {
	conn, err := rpc.Dial("tcp", "localhost:1234")
	if err != nil {
		log.Fatal("dialing: ", err)
	}

	var greeting string
	err = conn.Call("HelloWorldService.Greeting", "TDN", &greeting)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println(greeting)
}
