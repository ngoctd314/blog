# An introduction to Concurrency

## Why Is Concurrency Hard?

Concurrent code is notoriously difficult to get right. It usaully takes a few iterations to get it working as expected, and even then it's not uncommon for bugs to exist in code for years before some change in timing.

**Race conditions**

A race condition occurs when two or more operations must execute in the correct order, but the program has not been written so that this order is guaranteed to be maintained.

Most of the time, this shows up in what's called a data race, where one concurrent operation attempts to read a variable while at some undetermined time another concurrent operation is attempting to write to the same variable.

Example:

```go
var data int
go func() {
    // 3
    data++
}()

// 5
if data == 0 {
    fmt.Println("The value is : ", data)
}
```

Here, lines 3 and 5 are both trying to access the variable data, but there is no guarantee what order this might happen in.

This code can have 3 results:

+ data = 1 => no print
+ data = 0 => print 0
+ data = 0 => data++ => print 1

Most of the time, data races are introduced because the developers are thinking about the problem sequentially. They assume that because a line of code falls before another that it will run first.

Race conditions are one of the most insidious types of concurrency bugs because they may not show up until years after the code has been placed into production. Code seems to be behaving correctly, but in reality, there's just a very high chance that the operations will be executed in order. Sooner or later, the program will have an unintended consequence.

## Atomicity
