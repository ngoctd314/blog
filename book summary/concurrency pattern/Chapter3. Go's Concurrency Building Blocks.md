# Chapter 3. Go's Concurrency Building Blocks

## Goroutines

A goroutine is a function that is running concurrently (remember: not necessarily in parallel) alongside other code.

**So let's look at what's happening behind the scenes here: how do goroutines actually work? Are they OS thread? Green threads? How many can we create?**

They're not OS threads, and they're not exactly green threads - threads that are managed by a language's runtime - they're a higher level of abstraction known as coroutines. Coroutines are simply concurrent subroutines.

What makes goroutines unique to Go are their deep integration with Go's runtime. Go's runtime observes the runtime behavior of goroutines and automatically suspends them when they block and then resumes them when they become unblocked.

Go's mechanism for hosting goroutines is an implementation of what's called an M:N scheduler, which means it maps M green threads to N OS threads. Goroutines are then scheduled onto the green threads. When we have more goroutines that green threads avaiable, the scheduler handles the distribution of the goroutines across the avaiable threds and ensures that when these goroutines become blocked, other goroutines can be run.

Go follows a model of concurrency called the fork-join model. The work fork refers to the fact that at any point in the program, it can split off a child branch of execution to be run concurrently with its parent. The word join refers to the fact that at some point in the future, these concurrent branches of execution will join back together. Where the child rejoins the parent is called a join point.

Join points are what guarantee our program's correctness and remove the race condition.

We've been using a lot of anonymous functions in our examples to create quick goroutine examples. Let's shift our attention to closures. Closures close around the lexical scope they can created in, there by capturing variables. If you run a closure in a goroutine, does the closure operate on a copy of these variables, or the original references

```go
var wg sync.WaitGroup
salutation := "hello"
wg.Add(1)
go func() {
    defer wg.Done()
    salutation = "welcome"
}()
wg.Wait()
fmt.Println(salutation)
```

```go
var wg sync.WaitGroup
for -, salutation := range []string{"Hello", "greetings", "good day"} {
    wg.Add(1)
    go func() {
        defer wg.Done()
        fmt.Println(salutation)
    }()
}
wg.Wait()

// good day
// good day
// good day
```

That's kind of surprising! Let's figure out what's going on here. In this example, the goroutine is running a closure that has closed over the iteration variable solution, which has type of string. As our loop iterates, salutation is being assigned to the next string value in the slice literal. Because the goroutines being scheduled may run at any point in time in the future, it is undetermined what values will be printed from within the gorotine. On my machine, there is a high probability the loop will exit before the gorotines are begun. This means the salutation variable falls out of scope. What happens then? Can the goroutines still reference something that has potentially been garbage collected?

This is an interesting side not about how Go manages memory. The Go routine is observant enough to know that a reference to the salutation variable is still being held, and therefore will transfer the memory to the heap so that the goroutines can continue to access it.

A newly goroutine is given a few kilobytes, which is almost always enough. When it isn't, the run-time grows the memory for storing the stack automatically, allowing many goroutines to live in a modest amount of memory.

The garbage collector does nothing to collect goroutines that have been abandoned somehow

## The sync package

### WaitGroup

You can think of a WaitGroup like a concurrent-safe counter: calls to Add increment the counter by the interger passed in, and calls to Done decrement the counter by one. Calls to Wait block until the counter is zero.

### Mutex and RWMutex

Mutex stands for "mutual exclusion" and is a way to guard critical sections of your program. A critical section is an area of your program that requires exclusive access to shared resource. A Mutex provides a concurrent-safe way to express exclusive access to these shared resources.

You'll notice that we always call Unlock within a defer statement. This is a very common idiom when utilizing a Mutex to ensure that call always happens, even when panicing. Failing to do so will probably cause your program to deadlock.

Critical sections are so named because they reflect a bottleneck in your program. It is somewhat expensive to enter and exit a critical section, and so generally people attempt to minimize the time spent in critical sections.

One strategy for doing so is to reduce the cross-section of the critical section. There may be memory that needs to be shared between multiple concurrent processes, but perhaps not all of these processes will read and write to this memory. If this is the case, you can take advantage of a different type of mutex: sync.RWMutex

The sync.RWMutex is conceptually the same thing as Mutex: it guards access to memory; however, RWMutex gives you a little bit more control over the memory. You can request a lock for reading, in which case you will be granted access unless the lock is being held for writing. This means that an arbitrary number of reads can hold a reader lock so long as nothing else is holding a writer lock.

### Cond

An event is any arbitrary signal between two or more goroutines that carrier no information other than the fact that it has occurred. You'll want to wait for one of these signals before continuing execution on a goroutine. One naive approach to doing this is to use an infinite loop:

```go
// this will consume all cycles of one core
for conditionTrue() == false {}

// to fix that, we could introduce a time.Sleep
for conditionTrue() == false {
    time.Sleep(1*time.Milisecond)
}
```

This is better, but it's still inefficient, and you have to figure out how long to sleep for: too long or too short. It would be better if there were some kind of way for a goroutine to efficiently sleep until it was signaled to wake and check its condition. This is exactly what the Cond type does for us.

```go
// Here we instantiate a new Cond. The NewCond function takes in a type that satisfies the sync.Locker interface. This is what allows the
// Cond type to faciliate coordination with other goroutines in a concurrent-safe way.
c := sync.NewCode(&sync.Mutex{})
c.L.Lock()
for conditionTrue() == false {
    // wait to notified that the condition has occurred. This is a blocking call and the goroutine will be suspended
    c.Wait()
}
c.L.Unlock()
```

This approach is much more efficient. Note that the call to Wait doesn't just block, it suspends the current goroutine, allowing other goroutines to run on the OS thread.

Example: a goroutine that is waiting for a signal, and a goroutine that is sending signals. Say we have a queue of fixed length 2, and 10 items we want to push onto the queue. We want to enqueue items as soon as there is room, so we want to be notified as soon as there's room in the queue.

## Once

```go
	var count int

	increment := func() { count++ }

	var once sync.Once

	var increments sync.WaitGroup
	increments.Add(100)
	for i := 0; i < 100; i++ {
		go func() {
			defer increments.Done()
			once.Do(increment)
		}()
	}
	increments.Wait()

	fmt.Printf("Count is %d\n", count)
```

As the name implies, sync.Once is a type that utilizes some primitives internally to ensure that only one call to Do ever calls the function passed in - even on different goroutines.

```go
var count
increment := func() {count++}
decrement := func() {count --}

var once sync.Once
once.Do(increment)
once.Do(decrement)

fmt.Printf("Count: %d\n", count)

// Count: 1
```

sync.Once only counts the number of times Do is called, not how many times unique functions passed into Do are called.

## Channels

Channels are one of the synchronization primitives in Go. While they can be used to synchronize access of the memory, they are best used to communicated information between goroutines.