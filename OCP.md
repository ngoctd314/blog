# Open closed principle

Software entities (classes, modules, function, etc), should be open for extension, but closed for modification.

Open means that we should be able to extend or adapt code by adding new behaviors and features. Closed means that we should avoid making changes to existing code, changes that could result in bugs or other kinds of regression.

These two characteristics might seem contradictory, but the missing piece of the puzzle is the scope. When talking about being open, we are talking about the design or structure of the software. From this perspective, being open means that it is easy to add new packages, new interfaces, or new implementations of an existing interface.

When we talk about being closed, we are talking about existing code and minimizing the changes we make to it, particulary the APIs that are used by others.

**OCP helps reduce the risk of additions and extensions**

You can think of OCP as risk-mitigation strategy. Modifying existing code always has some risk involved, and changes to the code used by others especially so.

```go
func BuildOutput(response http.ResponseWriter, format string, person Person) {
    var err error
    switch format {
        case "csv":
            err = outputCSV(response, person)
        case "json":
            err = outputJSON(response, person)
        if err != nil {
            response.WriteHeader()
            return
        }
    }
    response.WriteHeader(200)
}
```

Just how much would have to change if we needed to add another format?

- We would need to add another case condition to the switch
- We need to write another formatting function. (this change is unavoidable)
- The caller of the method would have to be updated to use another format (this is the other unavoidable change)
- We would have to add another set of test scenarios to match the new formatting (this is also unavoidbale)

```go
func BuildOutput(response http.ResponseWriter, fotmatter PersonFormatter, person Person) {
    err := formatter.Format(response, person) 
    if err != nil {
        response.WriteHeader(http.StatusInternalServerError)
        return
    }
    response.WriteHeader(http.StatusOK)
}
```

How many changes was it this time? Let's see:

- We need to define another implementation of the PersonFormatter interface
- The caller of the method has to be updated to use the new format
- We have to write test scenarios for the new PersonFormatter

**OCP can help reduce the number of changes needed to add or remove a feature**

**OCP narrows the locality of bugs to only the new code and its usage**

## What does this mean for Go?

```go
type rowConverter struct {
}

// populate the supplied Person from *sql.Row or *sql.Rows object
func (d *rowConverter) populate(in *Person, scan func(dest ...interface{}) error) error {
    return scan(in.Name, in.Email)
}

type LoadPerson struct {
    // compose the row converter into this loader
    rowConverter
}

func (loader *LoadPerson) ByID(id int) (Person, error) {
    row := loader.loadFromDB(id)
    person := Person{}
    // call the compose "abstract class"
    err := loader.populate(&person, row.Scan)
    return person, err
}
```

We have extracted some of the shared logic into a rowConverter struct. Then by embedding that struct in the other structs, we can use it without changes. We hvae achieved the goals of the abstract class and OCP. Our code is open; we can embed wherever we like but closed. The embedded class has no knowledge of the fact that it was embedded.
