# Circuit Breaker

A switch to protect a circuit from damange caused by excessive current or short-circuit

From programming point of view, it is useful especially when one service calls another service. Service A regularly calls service b to get some data out of it and let's say sometimes what can happen when service B is down so it's either short of resources of it's having a bug or it's having a fatal.

## State

### Closed

The state called closed where nothing wrong is happening and all of calls from a to b is successful

### Open

When the failure threshold is reached that's when it enters this open state. Open mean service A cannot contact service b and that therefore service b.

### Half open
