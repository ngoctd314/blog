# Go trick

**Casting a function implement an interface**

```go
type iPrint interface {
	print(interface{})
}

type iPrintFunc func()

func (exec iPrintFunc) print(data interface{}) {
	exec()
	fmt.Println(data)
}

func casting(data interface{}, printer iPrint) {
	printer.print(data)
}

func main() {
	casting("hello world", iPrintFunc(fn))
}

func fn() {
	fmt.Println("exec")
}
```
