# Abstract Factory Design Pattern

Abstract Factory is a creational design pattern that lets you produce families of related objects without specifying their concrete classes.

## Solution

The first thing the Abstract Factory pattern suggests is to explicitly declare interfaces for each distinct product of the product family. Then you can make all variants of products follow those interfaces.

The next move is to declare the Abstract Factory - an interface with a list of creation methods for all products that are part of the product family. These methods must return abstract product types represented by the interfaces we extracted previously.

Now, how about the product variants? For each variant of a product family, we create a separate factory class based on the AbstractFactory interface. A factory is a class that return products of a particular kind.

The client code has to work with both factories and products via their respective abstract interfaces. The client shouldn't care about the concrete class of the factory it works with.

There's one more thing left to clarify: if the client is only exposed to the abstract interfaces, what creates the actual factory objects? Usually, the application creates a concrete factory object at the initialization stage. Just before that, the app must select the factory type depending on the configuration or the env settings.

## Structure

**1. Abstract Products** declare interfaces for a set of distinct but related products which make up a product family.

**2. Concrete Products** are various implements of abstract products, grouped by variants.

**3. The Abstract Factory interface** declares a set of methods for creating each of the abstract products.

**4. Concrete Factories** implement creation methods of the abstract factory. Each concrete factory corresponds to a specific variant of products and creates only those product variants.

**5. Although** concrete factories instantiate concrete products, signatures of their creation methods must return corresponding abstract product. This way the client code that uses a factory doesn't get coupled to the specific variant of the product it gets from factory. The Client can work with any concrete factory/product variant, as long as it communicates with their objects via abstract interfaces.
