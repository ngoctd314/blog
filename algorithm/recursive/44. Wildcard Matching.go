package main

func isMatch(s, p string) bool {
	lenS := len(s)
	lenP := len(p)
	var cacheDP = make(map[[2]int]bool)

	return dp(lenS-1, lenP-1, s, p, cacheDP)
}

func at(i, j int) [2]int {
	return [2]int{i, j}
}

func dp(i, j int, s, p string, cacheDP map[[2]int]bool) bool {
	if i < 0 && j < 0 {
		return true
	}
	if j < 0 {
		return false
	}
	if i < 0 {
		for j >= 0 {
			if p[j] != '*' {
				return false
			}
			j--
		}
		return true
	}

	if v, ok := cacheDP[at(i, j)]; ok {
		return v
	}
	if p[j] == '*' {
		cacheDP[at(i, j)] = dp(i-1, j, s, p, cacheDP) || dp(i, j-1, s, p, cacheDP)
		return cacheDP[at(i, j)]
	}
	if p[j] != s[i] && p[j] != '?' {
		cacheDP[at(i, j)] = false
		return false
	}
	cacheDP[at(i, j)] = dp(i-1, j-1, s, p, cacheDP)

	return cacheDP[at(i, j)]
}
