package main

import "fmt"

// ListNode ...
type ListNode struct {
	Val  int
	Next *ListNode
}

func addTwoNumbers(l1 *ListNode, l2 *ListNode) *ListNode {
	tmpL1 := []int{}
	tmpL2 := []int{}
	for l1.Next != nil {
		tmpL1 = append(tmpL1, l1.Val)
		l1 = l1.Next
	}
	tmpL1 = append(tmpL1, l1.Val)

	for l2.Next != nil {
		tmpL2 = append(tmpL2, l2.Val)
		l2 = l2.Next
	}
	tmpL2 = append(tmpL2, l2.Val)

	lenL1 := len(tmpL1) - 1
	lenL2 := len(tmpL2) - 1
	var ret = []int{}
	if lenL1 > lenL2 {
		k := 0
		rs := 0
		for lenL2 >= 0 {
			rs, k = add(tmpL1[lenL1], tmpL2[lenL2], k)
			ret = append(ret, rs)
			lenL1--
			lenL2--
		}
		for lenL1 >= 0 {
			rs, k = add(tmpL1[lenL1], 0, k)
			ret = append(ret, rs)
			lenL1--
		}
	} else {
		k := 0
		rs := 0
		for lenL1 >= 0 {
			rs, k = add(tmpL1[lenL1], tmpL2[lenL2], k)
			ret = append(ret, rs)
			lenL1--
			lenL2--
		}
		for lenL2 >= 0 {
			rs, k = add(tmpL2[lenL2], 0, k)
			ret = append(ret, rs)
			lenL2--
		}
	}

	r := &ListNode{
		Val:  ret[0],
		Next: nil,
	}
	for i := 1; i < len(ret); i++ {
		n := &ListNode{
			Val:  ret[i],
			Next: nil,
		}
		r.Next = n
	}

	for r.Next != nil {
		fmt.Println(r.Val)
		r = r.Next
	}

	return nil
}

func add(a, b, c int) (int, int) {
	s := a + b + c
	if s < 10 {
		return s, 0
	}
	return 0, s - 10
}
