package main

import "fmt"

/*
If a star is present in the pattern, it will be in the second position pattern[1]. Then we may ignore this part of the pattern,
or delete a matching character in the text. If we have a match on the remaining strings after of these operations, then the initial
inputs matched.
*/
func solve10WithRecursive(text, pattern string) bool {
	if len(pattern) == 0 {
		return len(text) == 0
	}
	firstMatch := len(text) > 0 && fmt.Sprint(text[:1]) == fmt.Sprint(pattern[:1]) || fmt.Sprint(pattern[:1]) == "."

	if len(pattern) >= 2 && fmt.Sprint(pattern[1:2]) == "*" {
		// ignore or more
		return solve10WithRecursive(text, fmt.Sprint(pattern[2:])) || len(text) > 0 && firstMatch && solve10WithRecursive(text[1:], fmt.Sprint(pattern))
	}

	return firstMatch && len(text) > 0 && solve10WithRecursive(fmt.Sprint(text[1:]), fmt.Sprint(pattern[1:]))
}

/*
DYNAMIC PROGRAMMING
As the problem has an optional substructure, it is natual to cache intermediate results
We ask the question dp(i, j): does text[i:] and pattern[j:] match?
*/
var cache = make(map[[2]int]bool)

func solveRegexDP(s, p string) bool {
	return regexDP(0, 0, s, p)
}
func regexDP(i, j int, s, p string) bool {
	if v, ok := cache[[2]int{i, j}]; ok {
		return v
	}
	if i >= len(s) && j >= len(p) {
		return true
	}
	if j >= len(p) {
		return false
	}

	matchFirst := i < len(s) && (s[i] == p[j] || p[j] == '.')
	if (j+1) < len(p) && p[j+1] == '*' {
		// don't use * || use *
		cache[[2]int{i, j}] = regexDP(i, j+2, s, p) || matchFirst && regexDP(i+1, j, s, p)
		return cache[[2]int{i, j}]
	}
	if matchFirst {
		cache[[2]int{i, j}] = regexDP(i+1, j+1, s, p)
		return cache[[2]int{i, j}]
	}
	cache[[2]int{i, j}] = false
	return false
}
