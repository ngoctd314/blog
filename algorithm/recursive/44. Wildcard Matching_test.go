package main

import "testing"

func Test_wildcardMatching(t *testing.T) {
	type args struct {
		s string
		p string
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{
			name: "",
			args: args{
				s: "aa",
				p: "*",
			},
			want: true,
		},
		{
			name: "",
			args: args{
				s: "abceb",
				p: "*a*b",
			},
			want: true,
		},
		{
			name: "",
			args: args{
				s: "aab",
				p: "c*a*b",
			},
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := isMatch(tt.args.s, tt.args.p); got != tt.want {
				t.Errorf("wildcardMatching() = %v, want %v", got, tt.want)
			}
		})
	}
}
