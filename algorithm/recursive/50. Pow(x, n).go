package main

func myPow(x float64, n int) float64 {
	if n < 0 {
		return 1 / myPow(x, -n)
	}

	if n == 0 {
		return 1
	}
	if n%2 == 0 {
		k := myPow(x, n/2)
		return k * k
	}
	k := myPow(x, n/2)

	return k * k * x
}
