package main

import (
	"testing"
)

func Test_solve10WithRecursive(t *testing.T) {
	type args struct {
		text    string
		pattern string
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{
			name: "",
			args: args{
				text:    "abc",
				pattern: "a.c",
			},
			want: true,
		},
		{
			name: "",
			args: args{
				text:    "aa",
				pattern: "a*",
			},
			want: true,
		},
		{
			name: "",
			args: args{
				text:    "aab",
				pattern: "c*a*b",
			},
			want: true,
		},
		{
			name: "#abc",
			args: args{
				text:    "ab",
				pattern: ".*c",
			},
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := solve10WithRecursive(tt.args.text, tt.args.pattern); got != tt.want {
				t.Errorf("solve10WithRecursive() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_solveRegexDP(t *testing.T) {
	type args struct {
		s string
		p string
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{
			name: "",
			args: args{
				s: "abc",
				p: "a.c",
			},
			want: true,
		},
		{
			name: "",
			args: args{
				s: "aa",
				p: "a*",
			},
			want: true,
		},
		{
			name: "",
			args: args{
				s: "aab",
				p: "c*a*b",
			},
			want: true,
		},
		{
			name: "#abc",
			args: args{
				s: "ab",
				p: ".*c",
			},
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := solveRegexDP(tt.args.s, tt.args.p); got != tt.want {
				t.Errorf("solveRegexDP() = %v, want %v", got, tt.want)
			}
		})
	}
}
