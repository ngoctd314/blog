package recursive

func reverseString(s []byte) []byte {
	if len(s) == 1 {
		return s
	}

	return append([]byte{s[len(s)-1]}, reverseString(s[:len(s)-1])...)
}
