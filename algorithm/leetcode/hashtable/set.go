package main

// Set ...
type Set map[string]int

// NewSet ...
func NewSet() Set {
	return make(Set)
}

// Add ...
func (s Set) Add(keys ...string) {
	for _, key := range keys {
		s[key]++
	}
}

// Delete ...
func (s Set) Delete(keys ...string) {
	for _, key := range keys {
		if _, ok := s[key]; ok {
			delete(s, key)
		}
	}
}

// Exist ...
func (s Set) Exist(key string) bool {
	_, ok := s[key]
	return ok
}
