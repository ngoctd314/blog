package main

import (
	"reflect"
	"testing"
)

func Test_solve30(t *testing.T) {
	type args struct {
		s  string
		in []string
	}
	tests := []struct {
		name string
		args args
		want []int
	}{
		{
			name: "",
			args: args{
				s:  "barfoothefoobarman",
				in: []string{"foo", "bar"},
			},
			want: []int{},
		},
		{
			name: "",
			args: args{
				s:  "wordgoodgoodgoodbestword",
				in: []string{"word", "good", "best", "word"},
			},
			want: []int{},
		},
		{
			name: "",
			args: args{
				s:  "barfoofoobarthefoobarman",
				in: []string{"bar", "foo", "the"},
			},
			want: []int{},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := solve30(tt.args.s, tt.args.in); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("solve30() = %v, want %v", got, tt.want)
			}
		})
	}
}
