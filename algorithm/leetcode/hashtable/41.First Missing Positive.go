package main

import "fmt"

func firstMissingPositive(nums []int) int {
	ret := 1
	s := NewSet()
	max := 1
	for _, v := range nums {
		s.Add(fmt.Sprint(v))
		if max < v {
			max = v
		}
	}
	for i := 1; i <= max+1; i++ {
		if !s.Exist(fmt.Sprint(i)) {
			ret = i
			break
		}
	}

	return ret
}
