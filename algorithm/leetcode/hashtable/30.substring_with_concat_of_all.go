package main

import "fmt"

func main() {

}

func solve30(s string, in []string) []int {
	var ret = make([]int, 0)
	setFromIn := NewSet()
	setFromIn.Add(in...)
	lWord := len(in[0])
	lIn := len(in)

	lens := len(s) - lWord*lIn
	if lens < 0 {
		fmt.Println("invalid input")
		return ret
	}

	for i := 0; i < lens+1; i++ {
		s := buildSet30(s[i:lWord*lIn+i], lWord, lIn, setFromIn)
		flag := true
		for k, v := range setFromIn {
			if s[k] != v {
				flag = false
				break
			}
		}
		if flag && len(s) > 0 {
			ret = append(ret, i)
		}
	}

	return ret
}

func buildSet30(s string, lWord int, lIn int, set Set) Set {
	ret := NewSet()
	for j := 0; j < lIn; j++ {
		if set.Exist(s[j*lWord : lWord*(j+1)]) {
			ret.Add(s[j*lWord : lWord*(j+1)])
		} else {
			return ret
		}
	}

	return ret
}
