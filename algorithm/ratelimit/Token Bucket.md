# Token Bucket Algorithm

The token bucket algorithm is widely used for rate limiting. It is simple, well understood and commonly used by internet companies.

The token bucket algorithm work as: A token bucket is a container that has pre-defined capacity. Tokens are put in the bucket at preset rates periodically. Once the bucket is full, no more tokens are added.

The token bucket algorithm takes two parameters:

- Bucket size: the maximum number of tokens allowed in the bucket
- Refill rate: number of tokens put into the bucket every second

How many buckets do we need? This varies, and it depends on the rate-limiting rules.

- It is usually necessary to have different buckets for different API endpoints. For instance, if a user is allowed to make 1 post per second, add 150 friends per day, and like 5 posts per second, 3 buckets are required for each user.
- If we need to throttle requests based on IP addresses, each IP address requires a bucket.
- If the system allows a maximum of 10000 requests persecond, it makes sense to have a global bucket shared by all requests.

**Pro**

- The algorithm is easy to implement
- Memory efficient
- Token bucket allows to burst of traffic for short periods. A request can go through as long as there are tokens left.

**Cons**

- Two parameters in the algorithm are bucket size and token refill rate. However, it might be challenging to tune them properly.
