package ratelimit

import "sync/atomic"

type bucket struct {
	token int64
}

func (b *bucket) refill(delta int64) {
	atomic.AddInt64(&b.token, delta)
}

func (b *bucket) isEnoughToken() bool {
	remainToken := atomic.LoadInt64(&b.token)
	if remainToken > 0 {
		atomic.AddInt64(&b.token, -1)
		return true
	}

	return false
}
