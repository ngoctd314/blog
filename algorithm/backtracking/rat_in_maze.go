package main

import "fmt"

/*
1. Create a solution matrix, initially filled with 0
2. Create a recursive function, which takes initial matrix, output matrix and position of rat (i,j)
3. if the position is out of the matrix or the position is not valid then return
4. Mark the position output[i][j] as 1 and check if the current position is destination or not. If destination is reached print the
output matrix and return
*/

func ratInMaze() {
	var board = [][]int{
		{1, -1, -1, -1},
		{0, 0, -1, 0},
		{-1, 0, -1, -1},
		{0, 0, 0, 0},
	}
	solveRatInMaze(board, 0, 0)
	for _, v := range board {
		fmt.Println(v)
	}

}

func solveRatInMaze(board [][]int, x, y int) bool {
	if x == 3 && y == 3 {
		return true
	}
	var moves = [][]int{{1, 0}, {-1, 0}, {0, 1}, {0, -1}}

	for _, move := range moves {
		moveX := move[0] + x
		moveY := move[1] + y
		if isSafeRatInMaze(board, moveX, moveY) {
			board[moveX][moveY] = 1
			if solveRatInMaze(board, moveX, moveY) {
				return true
			}
			board[moveX][moveY] = 0
		}
	}

	return false
}

func isSafeRatInMaze(board [][]int, x, y int) bool {
	return x >= 0 && y >= 0 && x < 4 && y < 4 && board[x][y] == 0
}
