package main

import "fmt"

func nQueen() {
	m := [][]int{
		{0, 0, 1, 0},
		{0, 0, 0, 0},
		{0, 0, 0, 0},
		{0, 0, 0, 0},
	}

	if solveNQueen(m, 0) {
		for _, v := range m {
			fmt.Println(v)
		}
	}
}

func solveNQueen(m [][]int, col int) bool {
	if col >= 4 {
		return true
	}
	for i := 0; i < 4; i++ {
		if isSafeNQueen(m, i, col) {
			m[i][col] = 1
			if solveNQueen(m, col+1) {
				return true
			}
			m[i][col] = 0
		}
	}
	return false
}

func isSafeNQueen(m [][]int, x, y int) bool {
	for i := 0; i < x; i++ {
		if m[i][y] == 1 {
			return false
		}
	}
	for i := 0; i < y; i++ {
		if m[x][i] == 1 {
			return false
		}
	}

	for i, j := x-1, y-1; i >= 0 && j >= 0; {
		if m[x][y] == 1 {
			return false
		}
		i--
		j--
	}

	return true
}
