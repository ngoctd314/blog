package main

import "fmt"

func knight() {
	board[0][0] = 0
	solution(1, 0, 0)

	for _, v := range board {
		fmt.Println(v)
	}
}

var moves = [][]int{
	{-2, 1},
	{-2, -1},
	{-1, -2},
	{-1, 2},
	{1, -2},
	{1, 2},
	{2, -1},
	{2, 1},
}

var board [][]int = [][]int{
	{-1, -1, -1, -1, -1, -1, -1, -1},
	{-1, -1, -1, -1, -1, -1, -1, -1},
	{-1, -1, -1, -1, -1, -1, -1, -1},
	{-1, -1, -1, -1, -1, -1, -1, -1},
	{-1, -1, -1, -1, -1, -1, -1, -1},
	{-1, -1, -1, -1, -1, -1, -1, -1},
	{-1, -1, -1, -1, -1, -1, -1, -1},
	{-1, -1, -1, -1, -1, -1, -1, -1},
}

var n int = 8

func solution(movei, x, y int) bool {
	// base condition
	if movei == n*n {
		return true
	}

	for _, move := range moves {
		movex := x + move[0]
		movey := y + move[1]
		if isValidMove(movex, movey) {
			board[movex][movey] = movei

			if solution(movei+1, movex, movey) {
				return true
			}
			// backtrack
			board[movex][movey] = -1
		}
	}

	return false
}

func isValidMove(x, y int) bool {
	return x >= 0 && x < n && y >= 0 && y < n && board[x][y] == -1
}
