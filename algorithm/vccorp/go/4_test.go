package main

import "testing"

func Test_ex4(t *testing.T) {
	type args struct {
		pattern string
		text    string
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{
			name: "",
			args: args{
				pattern: "abab",
				text:    "abab",
			},
			want: true,
		},
		{
			name: "",
			args: args{
				pattern: "?b",
				text:    "ab",
			},
			want: true,
		},
		{
			name: "",
			args: args{
				pattern: "*a",
				text:    "a",
			},
			want: true,
		},
		{
			name: "",
			args: args{
				pattern: "*a",
				text:    "ab",
			},
			want: false,
		},
		{
			name: "",
			args: args{
				pattern: "*",
				text:    "aa",
			},
			want: true,
		},
		// {
		// 	name: "",
		// 	args: args{
		// 		pattern: "ababab",
		// 		text:    "ab*b",
		// 	},
		// 	want: true,
		// },
		// {
		// 	name: "",
		// 	args: args{
		// 		pattern: "",
		// 		text:    "*",
		// 	},
		// 	want: true,
		// },
		// {
		// 	name: "",
		// 	args: args{
		// 		pattern: "aaaaab",
		// 		text:    "*?*b",
		// 	},
		// 	want: true,
		// },
		// {
		// 	name: "",
		// 	args: args{
		// 		pattern: "aa?b",
		// 		text:    "aacb",
		// 	},
		// 	want: true,
		// },
		// {
		// 	name: "",
		// 	args: args{
		// 		pattern: "aa?b",
		// 		text:    "aab",
		// 	},
		// 	want: false,
		// },
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := ex4(tt.args.pattern, tt.args.text); got != tt.want {
				t.Errorf("ex4() = %v, want %v", got, tt.want)
			}
		})
	}
}
