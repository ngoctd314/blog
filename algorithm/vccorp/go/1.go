package main

import (
	"fmt"
	"sort"
)

func ex1(s []int) []int {
	ret := []int{}
	lenS := len(s)

	copyS := []int{}
	for _, v := range s {
		copyS = append(copyS, v)
	}

	sort.Slice(copyS, func(i, j int) bool {
		return copyS[i] < copyS[j]
	})

	c := 0
	j := 0
	for i := 0; i < lenS-c; i++ {
		for s[i] != copyS[j] {
			ret = append(ret, s[i])
			i++
		}
		j++
		c++
	}
	fmt.Println(ret)

	return ret
}
