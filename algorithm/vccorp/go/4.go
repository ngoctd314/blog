package main

// this is base case for .
func matchOne(pattern, text string) bool {
	// '.' != a
	if len(text) == 0 {
		return false
	}

	// '.' == 'a'
	if pattern == "?" {
		return true
	}

	// 'a' == 'a'
	return pattern == text
}

func matchStar(pattern, text string) bool {
	// match >= 1 || match 0
	// match(pattern[:1], text[:1]) ||
	if len(pattern) != 0 {
		return match(pattern[1:], text)
	}
	if len(text) != 0 {
		return match(pattern, text[1:])
	}
	return false
}

func match(pattern string, text string) bool {
	if pattern == text {
		return true
	}

	if len(pattern) == 0 {
		return false
	}
	if pattern[:1] == "*" {
		return matchStar(pattern, text)
	}

	if len(text) == 0 {
		return false
	}
	return matchOne(pattern[:1], text[:1]) && match(pattern[1:], text[1:])
}

func ex4(pattern, text string) bool {
	return match(pattern, text)
}

// func isMatchAny(pattern, text string) bool {
// 	if pattern == "*" {
// 		return true
// 	}
// }
