package main

import (
	"reflect"
	"testing"
)

func Test_ex1(t *testing.T) {
	type args struct {
		s []int
	}
	tests := []struct {
		name string
		args args
		want []int
	}{
		{
			name: "",
			args: args{
				s: []int{6, 1, 2, 7, 8, 3, 4, 5, 9},
			},
			want: []int{6, 7, 8},
		},
		{
			name: "",
			args: args{
				s: []int{1, 2, 7, 3, 4, 5, 6, 8, 9},
			},
			want: []int{7},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := ex1(tt.args.s); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ex1() = %v, want %v", got, tt.want)
			}
		})
	}
}
