package main

import "testing"

func Test_ex3(t *testing.T) {
	type args struct {
		arr []int
		k   int
	}
	tests := []struct {
		name string
		args args
	}{
		{
			name: "",
			args: args{
				arr: []int{1, 2, 3, 4, 5, 6, 7, 8, 9},
				k:   8,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			ex3(tt.args.arr, tt.args.k)
		})
	}
}
