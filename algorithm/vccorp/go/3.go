package main

import "fmt"

func ex3(arr []int, k int) {
	s := make(map[int]map[int][]int)

	// init
	s[1] = make(map[int][]int)
	for i, v := range arr {
		s[1][i] = append(s[1][i], v)
	}

	lenIn := len(arr)
	// O(n)*O(n)*O(m)
	for i := 2; i <= lenIn; i++ {
		s[i] = make(map[int][]int)
		// prev solution
		m := 0
		for _, v1 := range s[i-1] {
			for _, v := range arr {
				if sumArr(v1)+v <= k {
					s[i][m] = append(v1, v)
					m++
				}
			}
		}

		if len(s[i]) == 0 {
			break
		}
	}

	for k, v := range s {
		fmt.Println(k, v)
	}

}

func sumArr(s []int) int {
	r := 0
	for _, v := range s {
		r += v
	}
	return r
}
