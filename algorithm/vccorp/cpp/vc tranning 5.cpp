#include <bits/stdc++.h>

using namespace std;

int n, k;
int ret = 0;
vector<int> arr;

void dfs(int sum, vector<int> arr, vector<int> sol, int i) {
  if (sum < k) {
    int s = sol.size();
    for (int i = 0; i < s; i++) {
      cout << sol[i] << " ";
    }
    cout << endl;
    ret++;
    return;
  }
  if (i >= arr.size() || sum >= k) {
    return;
  }

  sol.push_back(arr[i]);
  dfs(sum + arr[i], arr, sol, i);
  sol.pop_back();
  dfs(sum, arr, sol, i + 1);
}
int main() {
  cin >> n >> k;
  int i;
  while (n--) {
    cin >> i;
    arr.push_back(i);
  }
  vector<int> sol;
  dfs(0, arr, sol, 0);

  cout << ret;
}
