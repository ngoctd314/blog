#include<bits/stdc++.h>

using namespace std;

int n,k;

int main() {
    cin >> n >> k;
    vector<int> ar;
    int ai;
    int ret = 0;

    while(n--) {
        cin >> ai;
        if (ai < k) {
            ar.push_back(ai);
            ret++;
        }
    }
    int m = ret;
    int count = pow(2,m);
    for (int i = 0; i < count; i++) {
        int sum = 0;
        for (int j = 0; j < m; j++) {
            if ((i & (1 << j)) != 0) {
                sum += ar[j];
            }
        }
        if (sum < k) {
            ret ++;
        }
    }


    cout << ret;

}