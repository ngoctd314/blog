#include <bits/stdc++.h>

using namespace std;

int n, m;
int maxHist(vector<int> row) {
    // create an empty stack
    // the stack holds indexes of hist[] array
    stack<int> st;

    int tp;
    int max_area = 0;

    int area = 0;
    int i = 0; 
    while (i < m) {
        if (st.empty() || row[st.top()] <= row[i]) {
            st.push(i++);
        } else {
            tp = st.top();
            st.pop();
            area = row[tp]*(st.empty()? i : i - st.top() - 1);
            max_area = max(max_area, area);
        }
    }
    while(!st.empty()) {
        tp = st.top();
        st.pop();
        area = row[tp]*(st.empty()? i : i - st.top() - 1);
        max_area = max(max_area, area);
    }

    return max_area;
}

int main() {
    cin >> n >> m;
    vector<vector<int>> arr;
    int k;
    for (int i = 0; i < n; i++) {
        vector<int> tmp;
        for (int j = 0; j < m; j++) {
            cin >> k;
            tmp.push_back(k);
        }
        arr.push_back(tmp);
    }

    int ret = maxHist(arr[0]);
      for (int i = 1; i < n; i++) {
        for (int j = 0; j < m; j++)
            if (arr[i][j])
                arr[i][j] += arr[i - 1][j];
 
        ret = max(ret, maxHist(arr[i]));
    }
 
    cout << ret;
}