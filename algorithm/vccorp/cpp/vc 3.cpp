#include <bits/stdc++.h>

using namespace std;

int max_row, max_col, k;

bool hasNext(vector<vector<int>> &arr, int row, int col) {
  return row >= 0 && row < max_row && col >= 0 && col < max_col &&
         arr[row][col] == 1;
}

bool Try(vector<vector<int>> &arr, vector<vector<int>> &visit, int row, int col,
         int row_d, int col_d) {
  if (row == row_d && col == col_d && arr[row][col] == 1) {
    visit[row][col] = 1;
    return true;
  }
  if (hasNext(arr, row, col) == true) {

    visit[row][col] = 1;
    if (Try(arr, visit, row + 1, col, row_d, col_d) == true) {
      return true;
    }
    if (Try(arr, visit, row - 1, col, row_d, col_d) == true) {
      return true;
    }
    if (Try(arr, visit, row, col + 1, row_d, col_d) == true) {
      return true;
    }
    if (Try(arr, visit, row, col - 1, row_d, col_d) == true) {
      return true;
    }
    visit[row][col] = 0;
    return false;
  }
  return false;
}

int main() {
  cin >> max_row >> max_col >> k;
  vector<vector<int>> arr;

  for (int i = 0; i < max_row; i++) {
    vector<int> row;
    int tmp;
    for (int j = 0; j < max_col; j++) {
      cin >> tmp;
      row.push_back(tmp);
    }
    arr.push_back(row);
  }
  while (k--) {
    vector<vector<int>> visit;
    for (int i = 0; i < max_row; i++) {
      vector<int> row;
      for (int j = 0; j < max_col; j++) {
        row.push_back(0);
      }
      visit.push_back(row);
    }

    int row, col, row_d, col_d;
    cin >> row >> col >> row_d >> col_d;
    if (Try(arr, visit, row, col, row_d, col_d) == true) {
      cout << "True" << endl;
    } else {
      cout << "False" << endl;
    }
  }
}
