#include <bits/stdc++.h>

using namespace std;

int main() {
  int n;
  cin >> n;
  int ai;
  int arr[n];
  int max_ret = 0;
  int retx = 0;
  int rety = 0;
  for (int i = 0; i < n; i++) {
    cin >> ai;
    arr[i] = ai;
  }
  for (int i = 0; i < n; i++) {
    set<int> s;
    int j = i + 1;
    s.insert(arr[i]);
    while (j < n) {
      if (s.find(arr[j]) != s.end()) {
        if (j - i > max_ret) {
          max_ret = j - i;
          retx = i;
          rety = j;
        }
        break;
      } else {
        // not found
        s.insert(arr[j]);
        j++;
      }
    }
    if (j - i > max_ret) {
      max_ret = j - i;
      retx = i;
      rety = j;
    }
  }
  for (int i = retx; i < rety - 1; i++) {
    cout << arr[i] << ",";
  }
  cout << arr[rety - 1];
}