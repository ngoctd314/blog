#include <bits/stdc++.h>

#define ll long long

using namespace std;

int main() {
  int n;
  cin >> n;
  int a[n];
  int i = 0;
  ll sum = 0;
  while (i < n) {
    cin >> a[i];
    sum += a[i];
    i++;
  }

  int min = INT_MAX;
  ll sumL = 0;
  int rs = 0;
  for (int i = 0; i < n; i++) {
    sumL += a[i];
    if (abs(sum - 2 * sumL) < min) {
      min = abs(sum - 2 * sumL);
      rs = i;
    }
  }

  cout << rs + 1;
}

//  i = 0;
//  int sumi = 0, sumj = 0;
//  while (i != j) {
//    if (a[i] + sumi - sumj < a[j] + sumj - sumi) {
//      sumi += a[i];
//      i++;
//    } else {
//      sumj += a[j];
//      j--;
//    }
//  }
//  if (a[i] + sumi - sumj < a[j] + sumj - sumi) {
//    cout << i + 1;
//  } else {
//    cout << i;
//  }
