#include<bits/stdc++.h>

using namespace std;

int main() {
    int n;
    cin >> n;
    int ar[n];
    int ai;
    for (int i = 0; i < n; i++) {
        cin >> ai;
        ar[i] = ai;
    }

    int ret = 0;
    int count = 0;
    for (int i = 0; i < n-1; i++) {
        if (ar[i] < ar[i+1]) {
            count ++;
        } else {
            ret += count;
            count = 1;
        }
    }
    cout << ret;

}