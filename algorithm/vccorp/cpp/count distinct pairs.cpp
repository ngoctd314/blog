#include <bits/stdc++.h>
using namespace std;

#define ll long long

int main() {
  ios_base::sync_with_stdio(false);
  cin.tie(NULL);

  int n;
  cin >> n;
  vector<ll> arr;
  ll arr_i;
  while (n--) {
    cin >> arr_i;
    arr.push_back(arr_i);
  }
  int k;
  cin >> k;
  ll arrk_i;
  vector<ll> arrk;
  while (k--) {
    cin >> arrk_i;
    arrk.push_back(arrk_i);
  }

  for (ll k : arrk) {

    unordered_set<ll> set;
    unordered_set<ll> visited;

    int count = 0;
    for (ll num : arr) {
      if (set.find(k - num) != set.end() &&
          visited.find(num) == visited.end()) {
        count++;
        visited.insert(num);
        visited.insert(k - num);
      }
      set.insert(num);
    }
    cout << k << " " << count << endl;
  }
}
