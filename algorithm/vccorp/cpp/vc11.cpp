#include <bits/stdc++.h>

using namespace std;

stack<char> st;

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);

    int n; cin >> n;
    string tmp;
    char tp;
    getline(cin, tmp);
    while(n--) {
        int count = 0;
        string str;
        getline(cin, str);
        int len = str.length();
        for(int i = 0; i < len; i++) {
            cout << str[i];
            if(str[i] == '{') {
                st.push('{');
            } else {
                if (st.empty()) {
                    count++;
                    continue;
                }
                tp = st.top();
                st.pop();
                if (tp != '}') {
                    count ++;
                }
            }
        }
        cout << count ;
    }

}