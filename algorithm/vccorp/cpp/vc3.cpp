#include<bits/stdc++.h>

using namespace std;

#define vt2d vector<vector<int>>

int n, m, k;
vt2d ar;
vt2d vt;
set<string>solved;

int is_safe(int r, int c) {
    if ( r >= 0 && c >= 0 && r < n && c < m ){
        return 1;
    }
    return 0;
}

string h(int r1, int c1, int r2, int c2) {
    return to_string(r1) + to_string(c1) + to_string(r2) + to_string(c2);
}

void cache(int r1, int c1, int r2, int c2) {
    string h1  = h(r1, c1, r2, c2);
    string h2  = h(r2, c2, r1, c1);
    solved.insert(h1);
    solved.insert(h2);
} 

int sol(int r1, int c1, int r2, int c2) {
    string hstr  = h(r1, c1, r2, c2);
    string hstr1  = h(r2, c2, r1, c1);
    if (solved.find(hstr) != solved.end() || solved.find(hstr1) != solved.end()) {
        return 1;
    }
    if (r1 == r2 && c1 == c2 && is_safe(r1,c1) && ar[r1][c1] == 1) {
        vt[r1][r1] = 1;
        return 1;
    }
    if (is_safe(r1,c1) && ar[r1][c1] == 1) {
        if ( vt[r1][c1] == 1) {
            return 0;
        }
        vt[r1][c1] = 1;
        if (sol(r1+1,c1,r2,c2)) {
            cache(r1+1,c1,r2,c2);
            return true;
        }
        if (sol(r1,c1+1,r2,c2)) {
            cache(r1,c1+1,r2,c2);
            return true;
        }
        if (sol(r1-1,c1,r2,c2)) {
            cache(r1-1,c1,r2,c2);
            return true;
        }
        if (sol(r1,c1-1,r2,c2)) {
            cache(r1,c1-1,r2,c2);
            return true;
        }

        vt[r1][c1] = 0;
        return 0;
    }
    return 0;
}



int main() {
    int ai;
    cin >> n >> m >> k;
    for (int i = 0; i < n; i++) {
        vector<int> tmp;
        vector<int> tmp1;
        for (int j = 0; j < m; j++)  {
            cin >> ai;
            tmp.push_back(ai);
            tmp1.push_back(0);
        }
        ar.push_back(tmp);
        vt.push_back(tmp1);
    }
  
    while(k--) {
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                vt[i][j] = 0;
            }
        }
        int r1,c1, r2, c2;
        cin >> r1 >> c1>> r2 >> c2;
        if( sol(r1,c1,r2,c2)) {
            cout << "True" << endl;
        }else {
            cout << "False" << endl;
        }
    }
}