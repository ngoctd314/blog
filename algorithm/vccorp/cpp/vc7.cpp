#include <bits/stdc++.h>

using namespace std;

vector<vector<int>> dp;
int finding(string &s, string &p, int n, int m) {
  if (n < 0 && m < 0)
    return 1;

  if (m < 0)
    return 0;

  if (n < 0) {
    while (m >= 0) {
      if (p[m] != '*')
        return 0;
      m--;
    }
    return 1;
  }

  if (dp[n][m] == -1) {
    if (p[m] == '*') {
      return dp[n][m] = finding(s, p, n - 1, m) || finding(s, p, n, m - 1);
    } else {
      if (p[m] != s[n] && p[m] != '?')
        return dp[n][m] = 0;
      else
        return dp[n][m] = finding(s, p, n - 1, m - 1);
    }
  }

  return dp[n][m];
}

bool isMatch(string s, string p) {
  dp.clear();

  dp.resize(s.size() + 1, vector<int>(p.size() + 1, -1));
  int ret  = finding(s, p, s.size() - 1, p.size() - 1);
  dp[s.size()][p.size()] = ret;

  return ret;
}


int main() {
  int k;
  cin >> k;
  string k_arr[k];
  int ret[k];
  string ki;
  string text;
  for (int i = 0; i < k; i++) {
    cin >> ki;
    k_arr[i] = ki;
    ret[i] = 0;
  }
  while (cin >> text) {
    auto it = remove_if(text.begin(), text.end(), [](char const &c) {
      if ( c == '-' ) {
        return 0;
      }
      return ispunct(c);
    });
 
    text.erase(it, text.end());

    for (int i = 0; i < k; i++) {
      if (isMatch(text, k_arr[i])) {
        ret[i]++;
      }
    }
  }
  for (int i = 0; i < k; i++) {
    cout << ret[i] <<endl;
  }
}