#include<bits/stdc++.h>
using namespace std;

#define ll long long

int main() {
    int m_tmp;
    int n;
    cin >> n;
    set<int>s;

    int ai;
    int st = 0;
    int ed = 0;
    int tmp = 0;
    int reti = 0;
    int retj = 0;

    bool flag = true;
    for(int i = 0; i < n; i++) {
        if (flag) {
            st = i;
            flag = false;
        }
        cin >> ai;
        if (s.find(ai) != s.end()) {
            s.clear();
            flag = true;
            if (tmp > m_tmp) {
                reti = st;
                retj = i;
                m_tmp = tmp;
            }
            tmp = 0;
        }else {
            s.insert(ai);
            tmp ++;
        }
    }

    cout << reti << retj << " ";
}