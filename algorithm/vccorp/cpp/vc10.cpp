#include <bits/stdc++.h>

using namespace std;

#define ll long long

ll n, m;
int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);
    
    cin >> n>> m;

    vector<ll> arr;

    ll ai;
    for (ll i = 0; i < n; i++) {
        cin >> ai;
        arr.push_back(ai);
    }

    sort(arr.begin(), arr.end(), greater <ll>());

    int i;
    while(m--) {
        cin >> i;
        cout << arr[i-1] << "\n";
    }
}