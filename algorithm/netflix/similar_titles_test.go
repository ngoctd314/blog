package netflix

import "testing"

func Test_solveSimilarTitles(t *testing.T) {
	type args struct {
		titles []string
	}
	tests := []struct {
		name string
		args args
		want bool
	}{{
		name: "",
		args: args{
			titles: []string{"abc", "acb", "bac"},
		},
		want: true,
	},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := solveSimilarTitles(tt.args.titles); got != tt.want {
				t.Errorf("solveSimilarTitles() = %v, want %v", got, tt.want)
			}
		})
	}
}
