package netflix

import "fmt"

/*
Solution:
1. For each title, compute a 26 element vector. Each element in this vector represents
the frequency of an English letter in the corresponding title. This frequency count will be represented as a string
delimited with # characters. This mapping will generate identical vectors for strings that
are anagrams

2. Use this vector as key to insert the titles into a Hash Map. All anagrams will be mapped to
the same entry in this Hash Map.
*/

func solveSimilarTitles(titles []string) bool {
	s := map[string]struct{}{}
	for _, v := range titles {
		s[similarTitles{}.hash(v)] = struct{}{}
	}

	return len(s) == 1
}

type similarTitles struct{}

func (s similarTitles) hash(str string) string {
	rs := ""
	a := [26]int{}
	for _, v := range str {
		a[v-'a']++
	}
	for _, v := range a {
		rs += fmt.Sprint(v) + "#"
	}
	return rs
}
