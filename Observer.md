# Observer Design Pattern

Observer is a behavior design pattern that lets you define a subscription mechanism to notify multiple objects about any events that happen to the object they're observing.

The Observer pattern provides a way to subscribe and unsubscribe to and from these events for any object that implements a subscriber interface.

## Solution

The object that has some interesting state is open called subject, but since it's also going to notify other objects about the changes to its state, we'll call it publisher. All other objects that want to track changes to the publisher's state are called subscribers.

The Observer pattern suggests that you add a subscription mechnism to the publisher class so individual objects can subcribe to or unsubscribe from a stream of events comming from that publisher. Whenever an important event happens to the publisher, it goes over its subscribers and calls the specific notification method on their objects.

Reals apps might have dozens of different subscriber classes that are interested in tracking events of the same publisher class.

## Conceptual Example

In the e-commerce website, items go out of stock from time to time. There can be customers who are interested in a particular item that went out of stock. There are three solutions to this problem:

1. The customer keeps checking the availability of the item at same frequency.
2. E-commerce bombards customers with all new item he is interested in and gets notified if the item is avaiable.

3. The customer subscribes only to the particular item he is interested in and gets notified if the item is avaiable. Also, multiple customers can subscribe to the same product.
