# Structs in Go

## Struct Types and Struct Type Literals

The size of a struct type is the sum of the sizes of all its field types plus the number of some padding bytes. The padding bytes are used to align the memory addresses of some fields.

The size of a zero-field struct type is zero

## Composite Literals Are Unaddressable But Can Take Addresses

Generrally, only addressable values can take addresses. But there is a syntatic sugar in Go, which allows us to take addresses on composite literals. A syntactic sugar is an exception in syntax to make programming convenient.

## About Struct Value Conversions

Values of two struct types S1 and S2 can be converted to each other's types, if S1 and S2 share the identical underlying type (ignore field tags).
