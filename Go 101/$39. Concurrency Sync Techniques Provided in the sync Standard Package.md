# Concurrency Sync Techniques Provied in the sync package

## The sync.Cond Type

The sync.Cond type provides an efficient way to do notifications among goroutines.

Each sync.Cond value holds a sync.Locker field with name L.

The *sync.Cond type has three method, Wait(), Signal() and Broadcast()

Each sync.Cond value maintains a FIFO (first in first out) waiting goroutine queue. For an addressable sync.Cond value c

- c.Wait() must be called when c.L is locked, otherwise, a c.Wait() will cause panic. A c.Wait() call will

1. first push the current caller goroutine into the waiting goroutine queue maintained by c
2. then call c.L.Unlock() to unlock/unhold the lock c.L
3. then make the current caller goroutine enter blocking state.

The caller goroutine will be unblocked by another goroutine through calling c.Signal() or c.Broadcase() later.

- a c.Signal() call will unblock the first goroutine in (and remove it from) the waiting goroutine queue maintained by c, if the queue is not empty
- a c.Broadcast() call will unblock all the goroutines in (and remove them from) the waiting goroutine queue maintained by c, if the queue is not empty

In an idomatic sync.Cond use case, generally, on goroutine waits for changes of a certain condition and some other goroutines change the condition and send notifications
