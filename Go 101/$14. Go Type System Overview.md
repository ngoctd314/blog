# Concept: Basic Types

## Concept: Composite Types

Go supports the following composite types:

- pointer types
- struct types
- function types
- container types

-- array types - fixed-length container types
-- slice type - dynamic-length and dynamic-capacity container types
-- map types - maps are associate arrays (or dictionaries). The standard Go compiler implements maps as hashtables.

- channel types - channels are used to synchronize data among goroutines (the green threads in Go).
- interface types - interfaces play a key role in reflection and polymorphism

## Syntax: Type Definitions

A new defined type and its respective source type in type definitions are two distinct types.

Two types defined in two type definitions are always two distinct types.

The new defined type and the source type will share the same underlying type.

Types can be defined within function bodies

## Syntax: Type Alias Declarations

**Type alias declaration** is one new kind of type declarations

There are two built-in type aliases in Go, byte (alias of uint8) and rune (alias of int32).

```go
type (
    Name = string
    Age = int
)

type table = map[string]int
type Table = map[Name]Age
```

## Underlying Types

In Go, each type has an underlying type:

- for built-in types, the respective underlying types are themselves
- for the Pointer type defined in the  unsafe standard code package, its underlying type is itself. 
- the underlying type of a non-defined type, which must be a composite type, is itself.

