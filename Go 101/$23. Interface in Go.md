# Interfaces in Go

Fundamentally, interface types make Go suport value boxing. Consequently, through value boxing, reflection and polymorphism get supported.

## What are interface types?

An interface type specifies a collection of method prototypes. In other words, each interface type defines a method set.

```go
// This is a non-defined interface type
interface {
    About() string
}

// ReadWriter is a defined interface type
type ReadWriter interface {
    Read(buf []byte) (n int, err error)
    Write(buf []byte) (n int, err error)
}

// Runnable is an alias of a non-defined interface type
type Runnable = interface {
    Run()
}
```

## What Are Implementation?

Take some arbitrary type, T which may or may not be an interface type, and an interface type I. If the method set of T is a super set of the method set of I, then we say type T implements interface I.

Implementations are all implicit in Go. The compiler does not require implementation relations to be specified in code explicitly. There is not an implements keyword in Go. Go compilers will check the implementation relations automatically as needed.

An interface type always implements itself. Two interface types with the same method set implement each other.

Note, as any method set is a super set of a blank method set, so any type implements any blank interface type. This is an important fact in Go.

The implicit implementation design makes it possible to let concrete types defines in other library packages, such as standard packages, passively implement some interface types declared in user packages. For example, if we declare an interface type as the following one, then the type DB and type Tx declare in the database/sql standard package will both implement the interface type automatically, for they both have the three corresponding methods specified in the interface.

```go
type DatabaseStorer interface {
    Exec(query string, args ...interface{}) (sql.Result, error)
    Prepare(query string) (*sql.Stmt, error)
    Query(query string, args ...interface{}) (*sql.Rows, error)
}
```

## Value Boxing

We can view each interface value as box to encapsulate a non-interface value. To box/encapsulate a non-interface value into an interface value, the type of the non-interface value must implement the type of the interface value.

In Go, if a type T implements an interface type I, then any value of type T can be implicitly converted to type I. In other words, any value of type T is assignable to values type I. When a T value is converted (assigned) to an I value

- If type T is a non-interface type, then a **copy** of the T value is boxed (or encapsulated) into the result (or destination) I value. The time complexity of the copy is O(n), where n is the size of copied T value.

- If type T is also an interface type, then a copy of the value boxed in the T value is boxed (or encapsulated) into the result (or destination) I value. The standard Go comipler makes an optimization here, so the time complexity of the copy is O(1), instead of O(n).

The type information of the boxed value is also stored in the result (or destination) interface value. (This will be futher explained below).

When a value is boxed in an interface value, the value is called the **dynamic value** of the interface value. The type of the dynamic value is called the **dynamic type** of the interface value.

## Polymorphism

Polymorphism is one key functionality provided by interfaces, and it is an important feature of Go.

When a non-interface value t of a type T is boxed in an interface value i of type I, calling a method specified by the interface type I on the interface value i will call the corresponding method declared for the non-interface type T. **Calling the method of an interface value will actually call the corresponding method of the dynamic value of the interface value.** For example, calling method i.m will call method t.m actually. With different dynamic values of different dynamic types boxed into the interface vaule, the interface value behaves differently. This is called polymorphism.

When method i.m is called, the method table in the implementation information stored in i will be looked up to find and call the corresponding method t.m. The method table is a slice and the lookup is just a slice element indexing, so this is quick.

Note, calling methods on a nil interface value will panic at run time, for there are no avaiable declared methods to be called.

```go
type Filter interface {
    About() string
    Process([]int) []int
}

// UniqueFilter is used to remove duplicate numbers
type UniqueFilter struct{}
func (UniqueFilter) About() string {
    return "remove duplicate numbers"
}
func(UniqueFilter) Process(inputs []int) []int {
    outs := make([]int, 0, len(inputs))
    pusheds := make(map[int]bool)

    for _, n := range inputs {
        if !pusheds[n] {
            pusheds[n] = true
            outs = append(outs, n)
        }
    }

    return outs
}

// MultipleFilter is used to keep only the numbers which are multiples
type MultipleFilter int
func (mf MultipleFilter) About() string {
    return fmt.Sprintf("keep multiples of %v", mf)
}
func (mf MultipleFilter) Process(inputs []int) []int {
    var outs = make([]int, 0, len(inputs))
    for _, n := range inputs {
        if n % int(mf) == 0 {
            outs = append(outs, n)
        }
    }
    return outs
}
```

## Reflection

They dynamic type information stored in an interface value can be used to inspect the dynamic value of the interface value and manipulate the values referenced by the dynamic value. This is called reflection in programming.

## Type assertion

There are four kinds of interface-value involving value conversion cases in Go:

1. convert a non-interface value to an interface value, where the type of the non-interface value must implement the type of the interface value.
2. convert an interface value to an interface value, where type type of the source interface value must implement the type of the destination interface value.
3. convert an interface value to a non-interface value, where the type of the non-interface value must implement the type of the interface value.
4. convert an interface value to an interface value, where the type of the source interface value doesn't implement the destination interface type, but the dynamic type of the source interface value might implement the destination interface type.

The form of a type assertion expression is i.(T) where i is an interface value and T is a type name of a type literal. Type T must be

- either an arbitrary non-interface type
- or an arbitrary interface type

A type assertion might succeed or fail

- In the case of T being a non-interface type, if the dynamic type of i exists and is identical to T, then the assertion will succeed, otherwise will succeed, otherwise, the assertion will fail. When the assertion succeeds, the evaluation result of the assertion is a copy of the dynamic value of i.
- In the case of T being an interface type, if the dynamic type of i exists and implements T, then the assertion will succeed, otherwise, the assertion will fail. When the assertion succeeds, a copy of the dynamic value of i will be boxed into a T value and the T value will be used as the evaluation result of the assertion.

## More About Interface in Go

**Interface type embedding**
An interface type can embed a type name which denotes another interface type. The final effect is the same as unfolding the method prototypes specified by the embedded interface type into the definition body of the embedding interface type.

**Comparisons involving interface values**

There are two cases of comparisons involving interface values:

1. Comparisons between a non-interface value and an interface value.
2. Comparisons between two interface values.

For the first case, the type of the non-interface value must implement the type (assume it is I) of the interface values, so the non-interface value can be converted to (boxed into) an interface value of I. Comparing two interface values is comparing their respective dynamic types and dynamic values

Two interface values are equal only if one of the follwowing conditions are satisfied

1. They are both nil interface values.
2. Their dynamic types are identical and comparable, and their dynamic values are equal to each other.

```go
func main() {
    var a, b, c interface{} = "abc", 123, "a+b+c"
    // A case of step 2
    fmt.Println(a == b) // false
    // A case of step 3
    fmt.Println(a == c) // true
}
```

## Pointer dynamic value vs. non-pointer dynamic value

The official Go compiler/runtime makes an optimization which makes boxing pointer values into interface values more efficient than boxing non-pointer values O(n) with n equal size of object. For small size values, the efficiency differents are small, but for large size values, the differences may be not small.

So please try to avoid boxing large size values, box their pointers instead.

## Values of []T can't be directly converted to []I, even if type T implements interface type I

Example, we need to convert a []string value to []interface{} type. There is no direct way to make the conversion. We must make the conversion manually in a loop

```go
func main() {
    words := []string{"Go", "is", "a", "high", "efficient", "language"}

    iw := make([]interface{}, 0, len(words))
    for _, w := range words {
        iw = append(iw, w)
    }
}
```

## Each method specified in an interface type corresponds to an implicit function

```go
type I interface {
	m(int) bool
}

type T string

func (t T) m(n int) bool {
	return len(t) > n
}

func main() {
	var i I = T("gopher")

	fmt.Println(i.m(5))

	fmt.Println(I.m(i, 5))

}
```
