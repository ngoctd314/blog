# Go Value Copy Costs

Value copying happens frequently in Go programming. Values assignments, argument passing and channel value send operations are all value copying involved. This article will talk about the copy costs of values of all kinds of types.

## Value Sizes

The size of a value means how many bytes the direct part of the value will occupy in memory. **The indirect underlying parts of a value don't contribute to the size of the value.**
