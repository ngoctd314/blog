# Context in Golang

A Context carries a deadline, a cancellation signal, and other values across API
Context's methods may be called by multiple goroutines simultaneously

```go
type Context interface {
    // Deadline returns the time when work done on behalf of this 
    // context should be canceled. Deadline returns ok == false when no
    // deadline is set.
    Deadline() (deadline time.Time, ok bool)

    // Done returns a channel that's closed when work done on behalf of
    // the context should be canceled
    Done() <-chan struct{}

    // If Done is not yet closed, Err returns nil
    // If Done is closed, Err returns a non-nil error explaining why:
    // Canceled if the context was canceled
    // or DeadlineExceed if the context's deadline passed
    // After Err returns a non-nil error, successive calls to Err return
    // the same error
    Err() error

    // Value returns the value associated with this context for key, or 
    // nil if no value is associated with key. Successive calls to Value
    // with the same key returns the same result.
    Value(key any) any
}
```
