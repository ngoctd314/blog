# Channel in Go

## Channel Introduction

Don't communicate by sharing memory, share memory by communicating

Communicating by sharing memory and sharing memory by communicating are two programming manners in concurrent programming. When goroutines communicate by sharing memory, we use traditional concurrency synchronization techniques, such as mutex locks, to protect the shared memory to prevent data races. We can use channels to implement sharing memory by communicating.

Go provides a unique concurrency synchronization technique, channel. Channels make goroutines share memory by communicating. We can view a channel as an internal FIFO(first in, first out) queue within a program.

Along with transferring values (through channels), the ownership of some values may also be transferred between goroutines. When a goroutine sends a value to a channel, we can view the goroutine releases the ownership of some values. When a goroutine receives a value from a channel, we can view the goroutine acquires the ownerships of some values.

One problem of channels is, the experience of programming with channels is so enjoyable and fun that programmers often even prefer to use channels for the scenarios which channels are not best for.

## Channel Value Comparisons

All channel types are comparable

Non-nil channel values are multi-part values. If one channel value is assigned to another, the two channels share the same underlying parts. In other words, those two channels represent the same internal channel object. Then result of comparing them is true.

## Detailed Explainations for Channel Operations

Channels will be classified into three categories:

1. nil channel
2. non-nil but closed channels
3. not-closed non-nil channels

|Operation|A nil channel|A closed channel|A not-closed non-nil channel|
|---|---|---|---|
|Close|panic|panic|succeed to close|
|Send value to|block for ever|panic|block or succeed to send|
|Receive value from|block for ever|never block|block or succeed to receive|

- Closing a nil or an already closed channel products a panic in the current goroutine
- Sending a value to a closed channel also produces a panic in the current goroutine
- Sending a value to or receiving value from a nil channel makes the current goroutine enter and stay in blocking state for ever

We can think of each channel consisting of three queues internally:

```go
type goroutine struct{
	status string
}
type chan struct {
	// The receiving goroutine queue, goroutine in this queue are all in blocking state and
	// waiting to receive values to that channel
	recv []goroutine

	// The sending goroutine queue, goroutine in this queue are all in blocking state and
	// waiting to send values to that channel
	send []goroutine

	// The value buffer queue. Its size is equal to the capacity of the channel.
	value [N]T
}
```

Each channel internally holds a mutex lock which is used to avoid data races in all kinds of operations.

**Goroutine R tries to receive a value from a not-closed non-nil channel**

The goroutine R will acquire the lock associated with the channel firstly, then do the following steps until one condition is satisfied.

1. If the value buffer of the channel is not empty

```go
// non-blocking operation
if len(ch.value) > 0 {
	// meaning len(ch.revc) == 0
	// the goroutine R will receive ( by unshifting ) a value from the value buffer queue.
	goroutineR = ch.value.unshift()
	
	// if the sending goroutine queue of the channel is also not empty
	if len(ch.send) > 0 {
		// a sending goroutine will be unshifted out of the sending goroutine queue
		// and resumed to running state again
		val := ch.send.unshift()

		// The value the just unshifted sending goroutine trying to send will be pushed into the value buffer queue of the channel
		ch.value = append(ch.value, val)
	}

	// The receiving goroutine R continues running.
}
```

2. If value buffer queue of the channel is empty and sending goroutine queue of the channel is not empty

```go
// In this case, the channel must be an unbuffered channel
// non-blocking operation
if len(ch.value) == 0 && len(ch.send) > 0 {
	// the receiving goroutine R will unshift a sending goroutine from the sending goroutine queue of the channel
	// and receive the value the just unshifted sending goroutine trying to send.
	goroutineR = ch.send.unshift()

	// the just unshifted sending goroutine will get unblocked and resumed to running state again
	
	// the goroutine R continues running.
}
```

3. If value buffer queue and the sending goroutine queue of the channel are both empty, the goroutine R will be pushed into the receving goroutine R will be pushed into the receiving goroutine queue of the channel and stay in blocking state. It may be resumed to running state when another goroutine sends a value to the channel later. For this scenario, the channel receive operation is called a blocking operation.

**Goroutine S tries to send a value to a not-closed non-nil channel**

The goroutine S will acquire the lock associated with the channel firstly, then do the following steps until one step condition is satisfied

1. If the receiving goroutine queue of the channel is not empty, in which case the value buffer queue of the channel must be empty, the sending goroutine S will unshift a receiving goroutine from the receiving goroutine queue of the channel and send the value to the just unshifted receiving goroutine. The just unshifted receiving goroutine will get unblocked and resumed to running state again. The sending goroutine S continues running.

2. Otherwise (the receiving goroutine queue is empty), if the value buffer of the channel is not full, in which case the sending goroutine queue must be also empty, the value the sending goroutine S trying to send will be pushed into the value buffer queue, and then sending goroutine S continues running.

3. If the receiving goroutine queue is empty and the value buffer queue of the channel is already full, the sending goroutine S will be pushed into the sending goroutine queue of the channel and enter (and stay in) blocking state. It may be resumed to running state when another goroutine receives a value from the channel later.

**A goroutine tries to close a not-closed non-nil channel**

Once the goroutine has acquired the lock of the channel, both of the following two steps will be performed by the following order.

1. If the receiving goroutine queue of the channel is not empty, in which case the value buffer of the channel must be empty, all the goroutine in the receiving goroutine queue of the channel will be unshifted one by one, each of them will receive a zero value of the element type of the channel and be resumed to running state.

2. If the sending goroutine queue of the channel is not empty, all the goroutines in the sending goroutine queue of the channel will be unshifted one by one and each of them will produce a panic for sending on a closed channel. This is the reason why we should avoid concurrent send and close operations on the same channel.

After a channel is closed, the values which have been already pushed into the value buffer of the channel are still there.

**After a non-nil channel is closed, channel receive operations on the channel will never block**

The values in the value buffer of the channel can still be received. The accompanying second optional bool return values are still true. Once all the values in the value buffer are taken out and received, infinite zero values of the element type of the channel will be received by any of the following receive operations on the channel. As mentioned above, the optional second return result of a channel receive operation is an untyped boolean value which indicated whether or not the first result (the received value) is sent before the channel is closed. If the second return result is false, then the first return result (the received value) must be a zero value of the element type of the channel.

**The fact**

- If the channel is closed, both its sending goroutine queue and receiving goroutine queue must be empty, but its value buffer queue may not be empty
- At any time, if the value buffer is not empty, then its receiving goroutine queue must be empty
- At any time, if the value buffer is not full, then its sending goroutine queue must be empty
- If the channel is buffered, then at any time, at least one of the channel's goroutine queues must be empty (sending, receiving or both)
- If the channel is unbuffered, most of the time one of its sending goroutine queue and the receiving goroutine queue must be empty, with one exception. The exception is that a goroutine may be pushed into both of the two queues when executing a select control flow code block.

## Use Case

**A simple request/response example**

```go
func main() {
	c := make(chan int) // an unbuffered channel
	go func(ch chan<- int, x int) {
		time.Sleep(time.Second)
		ch <- x*x // 9 is sent
	}(c, 3)

	go func(ch <-chan int) {
		// Block until 9 is received
		n := <-ch
		fmt.Println(n)
		time.Sleep(time.Second)
		done <- struct{}{}
	}(c)
	<- done
	fmt.Println("bye")
}
```

## Channel Element Values Are Transferred by Copy

When a value  is transferred from one goroutine to another goroutine, the value will be copied at least one time. If the transferred value ever stayed in the value buffer of the channel, then two copies will happen in the transfer process. One copy happens when the value is copied from the sender goroutine into the value buffer, the other happens when the value is copied from the value buffer to the receiver goroutine.

For the sandard Go compiler, the size of channel element type smust be smaller than 65536. However, generally, we shouldn't create channels with large-size element types, to avoid large copy cost in process of transfering values between goroutines. So if the passed value size if too large, it is best to use a pointer element type instead, to avoid a large value copy cost.

## About Channel and Goroutine Garbage Collections

A goroutine can only be garbage collected when it has already **exited**.
