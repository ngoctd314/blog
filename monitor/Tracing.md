# Tracing with opentelemetry (golang)

Package trace provides an implementation of the tracing part of the Otel API.

To participate in distributed traces a Span needs to be created for the operation being performed as part of a traced workflow.

```go
var tracer trace.Tracer

func init() {
    tracer = otel.Tracer("instrumentation/package/name")
}

func operation(ctx context.Context) {
    var span trace.Span
    ctx, span = tracer.Start(ctx, "operation")
    defer span.End()
}
```

## TracerProvider

```go
type TracerProvider interface {
    Tracer(instrumentationName string, opts ...TracerOption) Tracer
}

// NewNoopTracerProvider returns an implementation of TracerProvider that performs no operations. The Tracer and Spans created from the returned 
// TracerProvider also perform no operations
func NewNoopTracerProvider() TracerProvider
```

## SDK Installation

The SDK connects telemetry from the OpenTelemetry API to exporters. Exporters are packages that allow telemetry data to be emitted somewhere.

## Create an Exporter

The SDK connects telemetry from the OpenTelemetry API to exporters. Exporters are packages that allow telemetry data to be emitted somewhere.

## Creating a Resource

Telemetry data can be crucial to solving issues with a service. The catch is, you need a way to identify what service, or even what service instance, that data is coming from.

Any information you would like to associate with the telemetry data the SDK handles can be added to the returned Resource. This is done by registering the Resource with the TracerProvider.

## Tracer Provider

You have your application instrumented to produce telemetry data you have an exporter to send that data to the exporter, but how are they connected? This is where TracerProvider is used.

First you are creating a console exporter that will export to a file. You are then registering the exporter with a new TracerProvider. This is done with a BatchSpanProcessor when it is passed to the trace.WithBatcher option

## Propagators API

Cross-cutting concerns send their state to the next process using Propagator S, which are defined as objects used to read and write context data to and from messages exchanged by the applications. Each concern creates a set of Propagator S for every supported Propagator type.

Propagator S leverage the Context to inject and extract data for each cross-cutting concern, such as traces and Baggage.

The Propagators API currently defines one Propagator type:

- TextMapPropagator is a type that inject values into and extracts values from carriers as string key/value pairs.

### Carrier

A carrier is the medium used by Propagator S to read values from and write values to. Each specific Propagator type defines its expected carrier type, such as a string map or a byte array.

### Operations

Propagator S MUST define Inject and Extract operations, in order to write values to and read values from carriers respectively.

### Inject

Injects the value into a carrier. For example, into the headers of an HTTP request

Required arguments:

- A Context. The Propagator MUST retreive the appropriate value from the Context first, such as SpanContext, Baggage or another cross-cutting concern context.
- The carrier that holds the propagation fields. For example, an outgoing message or HTTP request.

### Extract

Extracts the value from an incoming request. For example, from the headers of an HTTP request.

If a value can not be parsed from the carrier, for a cross-cutting concern, the implementation MUST NOT throw an exception and MUST NOT store a new value in the Context, in order to preserve any previously existing valid value.

Required arguments:

- A Context
- The carrier that holds the propagation fields.

### TextMap Propagator

TextMapPropagator performs the injection and extraction of a cross-cutting concern value as string k/v into carriers.

The carrier of propagated data on both the client(injector) and server(extractor) side is usually an HTTP request.

### Global Propagators
