# Graphite vs prometheus

Prometheus and Graphite are open-source monitoring tools used to store and graph time series data. Prometheus is a "time series DBMS and monitoring system", while Graphite is a simpler "data logging and graphing tool for time series data". Both are open source and primarily used for system monitoring.

One of the key performance indicators of any system, application, product, or process is how certain parameters or data points perform over time. What if you want to monitor hits on an API endpoint or database latency in seconds?

Monitoring tools build around time series data need to do the following under a very high transaction volume:

1. Collect (or at least listen for) events, typically with a timestamp;
2. Efficiently store these events at volume;
3. Support queries of these events;
4. Offer graphical monitoring of these capabilities so that trends can be followed over time.

## Overview of Graphite

In a way, Graphite is simpler than Prometheus, with fewer features. It does precisely two things:

1. Store numeric time series data
2. Render graphs of this data

Data collection to Graphite is passive, meaning that applications sending it data need to be configured to send data to Graphite's carbon component.

## Prometheus Overview

While Graphite is a simple data logging and graphing tool, prometheus is a comprehensive system and service monitoring system. Prometheus is at once feature rich, it also has a more specific and narrow application.

Prometheus actively scrapes data, stores it and support queries, graphs, and alerts, provide endpoints to other API consumers like Grafana or event Graphite.
