# What is gRPC?

## Introduction to gRPC

An introduction to gRPC and protocol buffers.

This page introduces you to gRPC and protocol buffers. gRPC can use protocol buffers as both its IDL and as its underlying message interchange format.

## Overview

In gRPC, a client application can directly call a method on a server application on a different machine as if it were a local object, making it easier for you to create distributed applications and services. As in many RPC systems, gRPC is based around the idea of defining a service, specifying the methods that can be called remotely with their parameters and return types. On the server side, the server implements this interface and runs a gRPC server to handle client calls. On the client side, the client has a stub that provides the same methods as the server.

gRPC clients and servers can run and talk to each other in a variety of environments - from servers inside Google to your own desktop - an can be written in any of gRPC's support languages.

By default, gRPC uses Protocol Buffers, Google's mature open source mechanism for serializing structured data.

## Core concepts

gRPC lets you define four kinds of service method:

- Unary RPCs where the client sends a single request to the server and gets a single response back

```proto3
service Hello {
    rpc SayHello(HelloRequest) returns (HelloResponse);
}
```

- Server streaming RPCs where the client sends a request to the server and gets a stream to read a sequence of messages back. The client reads from the returned stream until there are no more messages. gRPC guarantees message ordering within an individual RPC call

```proto3
service ServerStreaming {
    rpc LotsOfReplies(HelloRequest) returns (stream HelloResponse)
}
```

- Client streamings RPCs

```proto3
service ClientStreaming {
    rpc LotsOfGreetings(stream HelloRequest) returns (HelloResponse);
}
```

- Bidirectional streaming RPCs where both sides send a sequence of messages using a read-write stream. The two streams operate independently, so clients and servers can read and write in whatever order they like

```proto3
service BidirectionalStreaming {
    rpc BidiHello(stream HelloRequest) returns (stream HelloResponse)
}
```

### Using the API

Starting from a service definition in a .proto file, gRPC provides protocol buffer compiler plugins that generate client and server-side code. gRPC users typically call these APIs on the client side and implement the corresponding API on the server side.

- On the server side, the server implements the methods declared by the service and runs a gRPC server to handle client calls. The gRPC infrastructure decodes incoming requests, executes service methods and encodes service responses.

- On the client side, the client has a local object known as stub that implements the same methods as the service. The client can then just call those methods on the local object, wrapping the parameters for the call in the appropriate protocol buffer message type - gRPC looks after sending the request(s) to the server and returning the server's protocol buffer response(s)

### gRPC lifecycle

### Dealines/Timeouts

gRPC allows client to specify how long they are willing to wait for an RPC to complete before the RPC is terminated with a DEALINE_EXCEEDED error. On the server side, the server can query to see if a particular RPC has timed out, or how much time if left to complete the RPC.

### RPC termination

### Cancelling an RPC

Either the client and server can cancel an RPC at any time. A cancellation terminates the RPC immediately so that no further work is done.

Changes made before a cancellation are not rolled back.

### Metadata

### Channels

A gRPC channel provides a connection to a gRPC server on a specified host and port. It is used when creating a client stub. Clients can specify channel arguments to modify gRPC's default behavior. A channel has state, including connected and idle.
