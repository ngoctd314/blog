# Introduction to gRPC

gRPC is a modern open source high performance Remote Procedure Call (RPC) framework that can run in any environment. It can efficiently connect services in and across data centers. It can efficiently connect services in and across data centers with pluggable support for load balancing, tracing, health checking and authentication. It is also applicable in last mile of distributed computing to connect devices, mobile applications and browsers to backend services.

**Simple service definition**
Define your service using Protocol Buffers, a powerful binary serialization toolset and language

**Start quickly and scale**
Install runtime and dev environments with a single line and also scale to millions of RPCs per second with the framework

**Works across languages and platforms**
Automatically generate idiomatic client and server stubs for your service in a variety of languges and platforms

**Bi-directional streaming and integrated auth**
Bi-directional streaming and fully integrated pluggable authentication with HTTP/2 based transport
