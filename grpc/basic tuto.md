# Basic tutorial

This example you'll learn how to:

- Define a service in a .proto file.
- Generate server and client using the protocol buffer compiler.
- Use the Go gRPC API to write a simple client and server for your service.

## Defining the service

```proto3
// Our first step is to define the gRPC service and the method request and response types
service RouteGuide {
    // Then you define rpc methods inside your service definition, specifying their request and response types.
    // gRPC lets you define four kinds of service method, all of which are used in the RouteGuide service
    //
    // A simple RPC where the client sends a request to the server using the stub and waits for a response to come back
    // just like a normal function call
    rpc GetFeature(Point) returns (Feature) {}

    // A server-side streaming RPC where the client sends a request to the server and gets a stream to read a sequence of messages back.
    // The client reads from the returned stream until there are no more messages.
    rpc ListFeatures(Retangle) returns (stream Feature) {}

    // A client-side streaming RPC where the client writes a sequence of messages and sends them to server. Once the client has
    // finished writing the messages, it waits for the server to read them all and return its response.
    rpc RecordRoute(stream Point) returns (RouteSummary) {}

    // A bidirectional streaming RPC where both sides send a sequence of messages using a read-write stream.
    // The two streams operate independently, so clients and servers can read and write in whatever order they like: 
    // The server could wait to receive all the client messages before writing its responses, or it could alternately read a message
    // then write a message, or some other combination of reads and writes. The order of messages in each stream is preserved.
    rpc RouteChat(stream RouteNote) returns (stream RouteNote) {}
}
```

Our .proto file also contains protocol buffer message type definitions for all the request and response types used in our service methods

```proto3
message Point {
    int32 longtitude = 1;
    int32 latitude = 2;
}
```

Command:

```bash
protoc \
# generate code which contains all the protocol buffer code to populate, serialized, and retrieve request and response message types
--go_out=. --go_opt=paths=source_relative \
# generage code which contains all the following 
# + An interface type (or stub) for clients to call with the methods defined in the service
# + An interface type for servers to implement, also with the methods defined in the service
--go-grpc_out=. --go-grpc_opt=paths=source_relative \
*.proto
```

## Creating the server

There are two parts to making our RouteGuide service do its job:

- Implementing the service interface generated from our service definition: doing the actual "work" of our service.
- Running a gRPC server to listen for requests from clients and dispatch them to the right service implementation.
