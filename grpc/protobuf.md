# Overview

Protocol buffers provide a language-neutral, platform-neutral, extensible mechanism for serializing structured data in a forward-compatible and backward-compatible way. It's like JSON, execpt it's smaller and faster, and it generates native language bindings.

Protocol buffers are a combination of the definition language (created in .proto files), the code that the proto compiler generates to interface with data, language-specific runtime libraries, and the serialization format for data that is written to a file (or sent aross a network connection).

## What Problems do Protocol Buffers Solve?

Protocol buffers provide a serialization format for packets of types, structured data that are up to a few megabytes in size. The format is suitable for bot ephemeral network traffic and long-term data storage. Protocol buffers can be extended with new information without invalidating existing data or requiring code to be updated.

Protobufs are used to extensively across all manner of services at Google and data within them may persist for some time, maintaining backwards compatibility is crucial. Protocol buffers allow for seamless support of changes, including the addition of new fields and the deletion of existing fields, to any protocol without breaking existing services.

## What are the Benefits of Using Protocol Buffers?

Protocol buffers are ideal for any situation in which you need to serialize structured, record-like, typed data in a language-neutral, platform-neutral, extensible manner. They are most often used for defining communications protocols (with gRPC) and for data storage.

Some of the advantages of using protocol buffers include:

- Compact data storage
- Fast parsing
- Avaiability in many programming languages
- Optimized functionality through automatically-genrated classes

## Updating Proto Definitions Without Updating Code

## When are Protocol Buffers not a Good Fit?

Protocol buffers do not fit all data.

## Protocol Buffers Definition Syntax

A field can also be of:

- A message type, so that you can nest parts of the definition
- An enum type, so you can specify a set of values to choose from
- A oneof type
- A map type

After setting optionally and field type, you assign a field number. Field numbers cannot be repurposed or reused. If you delete a field, you should reserve its field number to prevent someone from accidentally reusing the number.

## Language Guide (proto3)

### Define A Message Type

Define a search request message format, where each search request has a query string, the particular page of results you are interested in, and a number of results per page.

```proto
syntax = "proto3"; // specifies that you're using proto3 syntax, default is proto2 if you ignore

// message definition specifies three fields (name/value pairs)
message SearchRequest {
    string query = 1; // unique number. Used to identify your fields in the message binary format, and should not be changed
    // once message type is in use. Field numbers in the range 1 through 15 take one byte to encode, including the field number
    // and field's type. So you should reserve the numbers 1 through 15 for very frequently occurring message elements. You 
    // can use 1-2^29 - 1. Reserve number: 19000 - 19999
    int32 page_number = 2;
    int32 result_page_page = 3;
}
```

Meessage fields can be one of the following:

- singular: a well-formed message can have zero or one of this field (but not more than one). And this is the default field rule for proto3 syntax
- repeated: this field can be repeated any number of times (including zero).

### Reserved Fields

If you update a message type by entirely removing a field, or commenting it out, future users can reuse the field number when making their own updates to the type. This can cause severe issues if they later load old versions of the same .proto

```proto
message Foo {
    reserved 2, 15, 9 to 11;
    reserved "foo", "bar";
}
```

### Enumerations

```proto
message SearchRequest {
    string query = 1;
    int32 page_number = 2;
    int32 result_per_page = 3;
    enum Corpus {
        UNIVERSAL = 0; 
        // start = 0. Because: There must be a zero value, so that we can use 0 as a numeric default value
        // the zero value needs to be the first element, for compatibility with the proto2 WEB = 1;
        IMAGES = 2;
        LOCAL = 3;
        NEWS = 4;
        PRODUCTS = 5;
        VIDEO = 6;
    }
    Corpus corpus = 4;
}
```

```proto
message MyMessage {
    enums EnumAllowingAlisas {
        option allow_alias = true;r
        UNKNOWN = 0;
        STARTED = 1;
        RUNNING = 1;
    }
}

message MyMessage {
    enum EnumNotAllowingAlias {
        UNKNOW = 0;
        STARTED = 1;
        // RUNNING = 1; // cause a compile error
    }
}
```

## Tutorials

The example we're going to use is a very simple "address book" application that can read and write people's contact details to and from a file. Each person in the address book has a name, an ID and email address, and a contact phone number.
