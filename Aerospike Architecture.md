# Aerospike Architecture

[official document](https://docs.aerospike.com/server/architecture/overview)

Aerospike is a distributed, scalable database. The architecture has three key objectives:

- Create a flexible, scalable platform for web-scale applications
- Provide the robustness and reliability (as in ACID) expected from traditional databases
- Provide operational effieciency with minimal manual involvement

The Aerospike  architecture comprises three layer:

- Client layer: This cluster-aware layer includes open source client lib.
- Clustering and Data Distribution Layer: Manage cluster communications and automates fail-over, replication, re-balancing and data migration.
- Data Storage Layer: This layer reliably stores data in DRAM and Flash for fast retrieval.

**Client layer**

- Implement the Aerospike API
- Tracks nodes and knows where data is stored
- Implements its own TCP/IP connection pool
- Send query to nodes

This arch reduces transaction latency, offload work from the cluster, and eliminates work for the developer.

**Distribution Layer**

The Distribution layer is designed to eliminate manual operations with the systematic automation of all cluster management functions.
A

- Cluster Management Module

Tracks nodes in the cluster. The key algorithm is a Paxos-based gossip-voting process that determines which nodes are considered part of the cluster. Aerospike implements heartbeat to moniter inter-node connectivity

- Data Migration Module

**Data Storage Layer**
Aerospike is a key-value store with a schemaless data model. Within a namespace, data is subdivided into sets and records. Each record has an indexed key unique in the set, and one or more named bins.



## Hybrid Storage

Aerospike can store data on any of the following types of media and combinations thereof:

- Dynamic Random Access Memory (DRAM)
- NVMe of SSD
- Persistent Memory (PMEM)
- Traditional spinning media

The Hybrid Memory System contains indexes and data stored in each node, handles interaction with the physical storage, contains modules for autiomatically removing old data from the database, 

**About namespaces, records, and storage**

Different namespaces can have different storage engines.

### DRAM

Pure DRAM storage - without persistence - provides higher throughput. Although modern Flash storage is very high performance, DRAM has better performance at a much higher price point.

### SSD/FLASH

When a write (update or insert) has been received from the client, a latch is taken on the row to avoid two conflicting writes on the same record for this cluster
After write validation, in-memory representations of the record update on the master. The data to be written to a devices is placed in a buffer for writing. When the write buffer is full, it is queued to disk.

### PMEM

Aerospike Enterprise Edition 4.8 supports storing record data in PMEM.

## Data Storage

As a schemaless distributed database, Aerospike has a distinct data model for organizing and storing its data.

### Data model

The Aerospike database does not require a traditional RDBMS schema. Rather, the data model is determined through your use of the system. For example, if you wanted to add a new data type to a record, you would write that data type into the record without having to first update any schema.

|Component|Description|
|---|---|
|storage engine|with each ns, you can choose the specific type of storage: NVMe, Flash, DRAM, PMEM|
|ns|a ns is a collection of records that share one specific storage engine, with common policies such as replication factor, encryption and more. A database can contain multiple ns|
|record|record is a contiguos storage unit for all the data uniquely identified by a single key|
|set|records can be optionally grouped into sets. Sets are similar to tables in an RDBMS, but without an explicit schema.|
|bin|a record is subdivided into bins, which are similar to columns in an RDBMS. Each bin has its own data type, and those do not need to agree between records. Secondary indexes can optionally be declared on bins|

**Namespaces**
Namespaces are top-level data containers. The way you collect data in namespaces relates to how data is stored and managed. A namespaces contains record, indexes and policies. Policies dicate namesapce behavior, including:

- How data is physically stored
- How many replicas exist for a record
- When records expire

**Sets**

In ns, records can belong to an optional logical container called a set. Sets allow applications to logically group records in collections. Sets inherit the policies defined by their namespace. You can define additional policies or operations specific to the set. For example, secondary indexes can be specified for a particular set, or a scan operation can be done on a specific set.

Records in the namespace do not have to be in a set, and instead just belong to the namespace.

**Records**
A record is the basic unit of storage in the database. Records can belong to a namespace or to a set within the namespace. A single record is uniquely identified by a key. Records are composed of:

|Component|Decription|
|---|---|
|key|The unique identifier of the Record|
|metadata|Contains version information (generation count), the record's expiration (TTL), and last updated time (LUT)|
|bins|Bins store the record data. The data type of the bin is set by the value of the data it contains. Multiple bins (and data types) can be stored in a single record.|

### Primary index

### Secondary index

### Set indexes

## Distribution

## User-Defined Functions

## Client Architecture

This illustrates the Aerospike client-server architecture.

## CAP and ACID

The CAP Theorem postulates that only two of three (consistency, availability, and partition-tolerance) properties can be guaranteed in a distributed system at any time.