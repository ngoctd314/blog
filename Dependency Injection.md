# Dependency Injection (DI)

Do you want code that is easier to maintain? How about easier to test? Easier to extend? Dependency Injection (DI) might be just the tool you need.

## Why does DI matter?

DI is coding in such a way that those resources (that is, functions or structs) that we depend on are abstractions. Because these dependencies are abstract, changes to them do not necessitate changes to our code. The fancy word for this is decoupling.

In Golang, abstraction refer interface and function closure (function literals)

**DI reduces the knowledge required when working on a piece of code, by expressing dependencies in an abstract or generic manner**

Consider the following example is an interface and the SavePerson() function that uses it:

```go
// Saver persists the supplied bytes
type Saver interface {
    Save(data []byte) error
}

// SavePerson will save person
func SavePerson(person []byte, saver Saver) error {
    return saver.Save(person)
}
```

What does Saver do? It saves some bytes somewhare. How does it do this? We don't know and, while working on the SavePerson function, we don't care.

Let's look at another example that uses a function literal:

```go
// LoadPerson will load the requested person by ID
// Errors include: invalid ID, missing person and failure to load
// or decode
func LoadPerson(ID int, decodePerson func(data []byte) *Person) (*Person, error) {
    bytes, err := loadPerson(ID)
    return decodePerson(bytes), nil
}
```

What does decodePerson do? It converts the bytes into a person. How? We don't need to know to right now.

**DI enables us to test our code in isolation of our dependencies**

Now, let's say that the preceding code came from a system that stored data in Network File Share. How would we write unit tests for that? Having access to an NFS at all the time would be a pain.

On the other hand, by relying on an abstraction, we could swap out the code that sdaves to the NFS with fake code. This way, we are only testing our code in isolation from the NFS.

**DI enables us to quickly and reliably test situations that are otherwise difficult or impossible**

Considering the earlier example, how could we test our error-handling code? We could shut down the NFS through some external script every time we run the tests, but this would likely be slow and would definitely annoy anyone else that depend on it.

**DI reduces the impact of extensions or changes**

Let's not fotget about the traditional sales pitch for DI. Tomorrow, if we decided to save to a NoSQL database instead of our NFS, how would our SavePerson code have to change? Not one bit. We would only need to write a new Saver implementation, giving us fourth advantage of DI

## Code smell that indicate you might need DI

- Code bloat
- Resistance to change
- Wasted effort
- Tight coupling

## What is DI?

## When should I apply DI?

## How can I improve as a Go programmer?
