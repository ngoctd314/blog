# Composite

Composite is a structural design pattern that lets you compose objects into tree structures and the work with these structures as if they were individual objects.

Composite is a structural design pattern that allows composing objects into a tree-like structure and work with the it as if it was a singular object.
Composite became a pretty popular solution for the most problems that require building a tree structure. Composite's greate feature is the ability to run methods recursively over the whole tree structure and sum up the results.

## Problem

Using the Composite pattern makes sense only when the core model of your app can be represented as a tree.

For example, imagine that you have two types of objects: Products and Boxes. A Box can contain several Products as well as a number of smaller Boxes. These little Boxes can also hold some Products or even smaller Boxes, and so on.

Say you decide to create an ordering system that uses these classes. Orders could contain simple products without any wrapping, as well as boxes stuffed this products... and other boxes. How would you determine the total price of such an order?

An order might comprise various products, packaged in boxes, which are packaged in bigger boxes and so on. The whole structure looks like an upside down tree.

## Conceptual Example

In the filesystem, there are two types of objects: files and folders. There are cases when files and folders should be treated to be the same way. This is where the Composite pattern comes in handy.

Imagine that you need to run a search for a particular keyword in your file system. This search operation applies to both files and folders. For a file, it will just look into the contents of the file; for a folder, it will go through all files of the folder to find that keyword.
