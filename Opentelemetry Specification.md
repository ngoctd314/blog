# Specification

## OpenTelemetry Architecture

OpenTelemetry is designed as a set of independent observability tools, called Signals, which are build on top of a shared mechnism for context program.
Signals work as cross-writing concerns, mixed into many libraries.

At the highest architecture level, OpenTelemetry clients are organized into signals. Each signal provides a specialized form of observability. For example, tracing, metrics, and baggage are three separate signals. Signals share a common subsystem - context propagation - but they function independently from each other.

Each signal provides a mechnism for software to describe itself.

Otel clients are designed to separate the portion of each signal which must be imported as cross-cutting concerns from the portions which can be managed independently. Each signal consists of four types of packages: API, SDK, Semantic Conventions, and Contrib.

## Tracing Signal

A distributed trace is a set of events, triggered as a result of a single logical operation, consolidated arcoss various components of an application. A distributed trace contains events that cross process, network and security boundaries.

### Traces

Traces in Otel are defined implicitly by their Spans.

A span represents an operation within a transaction.

- An operation nam
- A start and finish timestamp
- Attributes: A list of key-value pairs
- Events, each of which is a tuple (timestamp, name, Atrributes)
- Parent's Span
- Links to zero or more Spans
- SpanContext information required to reference a Span

**SpanContext**

Represents all the information that identifies Span in the Trace and MUST be propagated to child Spans and across process boundaries. A SpanContext contains the tracings identifiers and the options that are propagated from parent to child Spans.

- TraceId is the identifier for a trace. TraceId is used to group all spans for a specific trace together arcoss all processes.
- SpanId is the identifier for a span.
- TraceFlags
- Tracestate carries tracing-system

## OpenTelemetry Client Design Principles

1. The Otel API must be well-defined and clearly decoupled from the implementation. This allows end users to consume API only without consuming the implementation

2. Third party libraries and frameworks that add instrumentation to their code will have a dependency only on the API of Otel client. The developers of third party libraries and frameworks do not care (and cannot know) what specific implementation of Otel is used in the final application.

## Common specification concepts

### Atrribute

An Atrribute is a key-value pair, which MUST have the following properties:

- The attribute key MUST be a non-nil and non-empty string
- The attribute value is either: primitive type or an array of primitive types

## Context

### Overview

A Context is a propagation mechnism which carries execution-scoped values across API
A Context must be immutable, and its write operations MUST result in the creation of a new Context
