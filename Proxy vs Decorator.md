# Different between proxy vs decorator

Here is the direct quote from the GoF

Although decorators can have similar implementations as proxies, decorators have a different purpose. A decorator adds one or more responsibilities to an object, whereas a proxy controls access to an object.
Proxies vary in the degree to which they are implemented like a decorator. A protection proxy might be implemented exactly like a decorator. On the other hand, a remote proxy will not contain a direct reference to its real subject but only an indirect reference, such as host ID and local address on host.

Popular answers indicate that a Proxy knows the concrete type of its delegate. From this quote we can see that is not always true.

The different between Proxy and Decorator according to the GoF is that Proxy restricts the client. Decorator does not. Proxy may restrict what a client does by controlling access to functionality; or it may restrict what a client knows by performing actions that are invisible and unknown to the client.

Decorator does the opposite: it enhances what its delegate does in a way that is visible to clients.

We might say that Proxy is a black box while Decorator is a white box.

Decorator informs and empowers its client.
Proxy restricts and disempowers its client.
