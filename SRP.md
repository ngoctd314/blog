# Single responsibility principle

A class should have one, and only one, reasone to change

**SRP reduces the complexity by decomposing code into smaller, more concise pieces**

```go
// before apply SRP
type caculator struct{}

func (c caculator) caculate() ( interface{}, error ) {
	return nil, nil
}

func (c caculator) output(writer io.Writer) {}

func (c caculator) outputCSV(writer io.Writer) {}

//after apply SRP
type caculator struct{}

func (c caculator) caculate() (interface{}, error) {
	return nil, nil
}

type printer interface {
	output(data interface{})
}

type defaultPrinter struct {
	writer io.Writer
}

func (p defaultPrinter) output(data interface{}) {}

type csvPrinter struct {
	writer io.Writer
}

func (p csvPrinter) output(data interface{}) {}
```

**SRP increases the potential reusability of code**

**SRP makes tests simpler to write and maintain**

**SRP is an excellent way to improve general code readability**

```go
// before apply SRP
var db *sql.DB

type Person struct {
	ID    string
	Name  string
	Phone string
}

func loadUserHandler(resp http.ResponseWriter, req *http.Request) {
	err := req.ParseForm()
	if err != nil {
		resp.WriteHeader(http.StatusInternalServerError)
		return
	}

	userID, err := strconv.ParseInt(req.Form.Get("UserID"), 10, 64)
	if err != nil {
		resp.WriteHeader(http.StatusPreconditionFailed)
		return
	}

	row := db.QueryRow("SELECT * FROM Users WHERE ID = ?", userID)
	person := &Person{}
	err = row.Scan(&person.ID, &person.Name, &person.Phone)
	if err != nil {
		resp.WriteHeader(http.StatusInternalServerError)
		return
	}

	encoder := json.NewEncoder(resp)
	encoder.Encode(person)
}

// after apply SRP
func loadUserHandler(resp http.ResponseWriter, req *http.Request) {
	userID, err := extractIDFromRequest(req)
	if err != nil {
		resp.WriteHeader(http.StatusPreconditionFailed)
		return
	}
	person, err := loadPersonByID(userID)
	if err != nil {
		resp.WriteHeader(http.StatusInternalServerError)
		return
	}
	outputPerson(resp, person)
}

```

## How does this relate to DI?

When applying DI to our code, we are unsurprisingly injecting our dependencies, typically in the form of the function parameter. If you see a function with many injected dependencies, this is a likely sign that the method is doing too much.

## Go packages
