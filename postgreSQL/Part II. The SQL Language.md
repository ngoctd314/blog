
## Chapter 4. SQL syntax

## Chapter 5. Data Definition

## Chapter 6. Data Manipulation

## Chapter 7. Queries

## Chapter 8. Data Types

## Chapter 9. Functions and Operators

## Chapter 10. Type Conversion

## Chapter 11. Indexes

## Chapter 12. Full Text Search

## Chapter 13. Concurreny Control

## Chapter 14. Performance Tips

## Chapter 15. Parallel Query
