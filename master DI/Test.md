# Test in Golang

Happy path: This is when everything goes as expected. The tests also tend to document how to use the code.
Input errors: incorrect and unexpected inputs can often cause the code to behave in strange ways.

## Table-driven tests

## Stubs

Stubs are fake implementations of a dependency, that is, an interface that provides a predictable, usually fixed result. Stubs are also used to help exercise code paths, such as errors, that otherwise might be very difficult or impossible to trigger.

```go
type PersonLoaderStub struct {
    Person *Person
    Error error
}

func (p *PersonLoaderStub) Load(ID int) (*Person,error) {
    return p.Person, p.Error
}

func TestLoadPersonName(t *testing.T) {
    fakeID := 1
    scenarios := []struct{
        desc string
        loaderStub *PersonLoaderStub
        expectedName string
        expectErr bool
    }{
        {
            desc: "happy path",
            loaderStub: &PersonLoaderStub{
                Person: &Person{Name: "Sophia"}
            },
            expectedName: "Sophia",
            expectErr: false,
        },
        {
            desc: "input error",
            loaderStub: &PersonLoaderStub{
                Error: ErrNotFound,
            },
            expectedName: "",
            expectErr: true,
        },
        {
            desc: "system error path",
            loaderStub: &PersonLoaderStub{
                Error: errors.New("something failed")
            },
            expectedName: "",
            expectErr: true,
        },
    }
}
```

## Mocks

Mocks are very much like stubs, but they have one fundamental difference. Mocks have expectations. When we used stubs, our tests did nothing to validate our usage of the dependency; with mocks they will.

## Test-induced damage

Changes to a system for the sole purpose of making testing easier or faster resulted in test-induced damage. 