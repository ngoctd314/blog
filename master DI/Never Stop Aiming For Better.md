# Never Stop Aiming for Better

Code easier to maintain, easier to test, easier to extend. DI might be just the tool you need.

## So, how do I define DI?

DI is coding in such a way that those resources (that is, functions or structs) that we depend on are abstractions. Because these dependencies are abstract, changes to them do not necessitate changes to our code. The fancy word for this is decoupling. In Go, abstraction can used in interface and function literals (also known as closures).

```go
// Saver persists the supplied bytes
type Saver interface {
    Save(data []byte) error
}

// SavePerson will validate and persist the supplied person
func SavePerson(person *Person, saver Saver) error {
    // validate the inputs
    err := person.validate()
    if err != nil {
        return err
    }

    // encode person to bytes
    bytes, err := person.encode()
    if err != nil {
        return err
    }

    // save the person and return the result
    return saver.Save(bytes)
}

// Person data object   
type Person struct {
    Name string
    Phone string
}

// validate the person object
func (p *Person) validate() error {
    if p.Name == "" {
        return errors.New("name missing")
    }
    if p.Phone == "" {
        return errors.New("phone missing")
    }
    return nil
}
// convert the person into bytes
func (p *person) encode() ([]byte, error) {
    return json.Marshal(p)
}
```

Q: What does Saver do?
A: It saves some bytes somewhere

Q: How does it do this?
A: We don't know and, while working on the SavePerson function, we don't care.

Let's look at another example that uses a function literal:

```go
// LoadPerson will load the requested person by ID
func LoadPerson(ID int, decodePerson func(data []byte) *Person) (*Person, error) {
    // validate the input
    if ID <= 0 {
        return nil, fmt.Errorf("invalid ID '%d' supplied", ID)
    }
    // load from storage
    bytes, err := loadPerson(ID)
    if err != nil {
        return nil, err
    }
    // decode bytes and return
    return decodePerson(bytes), nil
}
```

What does decodePerson do? It converts the bytes into a person. How? We don't need to know to right now.

**DI reduce the knowledge required when working on a piece of code, by expressing dependencies in an abstract or generic manner**

Now, let's say that the preceding code came from a system that stored data in a NFS. How would we write unit tests for that? Having access to an NFS at all times would be a pain.

On the other hand, by relying on an abstraction, we would swap out the code that saves to the NFS with fake code (mock).

```go
func TestSavePerson_happyPath(t *testing.T) {
    in := &Person{
        Name: "Sophia",
        Phone: "1234",
    }

    // mock the NFS
    mockNFS := &mockSaver{}
    mockNFS.On("Save", mock.Anything).Return(nil).Once()
    // Call save
    resultErr := SavePerson(in, mockNFS)
    // validate result
    assert.NoError(t, resultErr)
}
```

**DI enables us to test our code in isolation of our dependencies**

**DI enables us to quickly and reliably test situations that are otherwise difficult or impossible**

Let's not forget about the traditional sales pitch for DI. Tomorrow, if we decided to save to a NoSQL database instead of our NFS, how would our SavePerson code have to change? Not one bit. We would only need to write a new Saver implementation, giving us the fourth advantage of DI:

**DI reduces the impact of extensions or changes**

At the end of the day, DI is a tool-a handy tool, but no magic bullet. It's a tool that can make code easier to understand, test, extend and reuse-a tool that can also help reduce the circular dependency issues.

## Code smells that indicate you might need DI
