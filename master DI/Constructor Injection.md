# Constructor injection

When an object requires a dependency to work, the easiest way to ensure that dependency in always avaiable is to require all users to supply it as a parameter to the object's constructor. This is known as constructor injection.

Let's work through an example where we will extract a dependency, generalize it, and achieve constructor injection. We will send an email to new users when they sign up.

```go
// WelcomeSender sends a Welcome email to new users
type WelcomeSender struct {
    mailer *Mailer
}

func (w *WelcomeSender) Send(to string) error {
    body := w.buildMessage()
    return w.mailer.Send(to, body)
}

func NewWelcomeSender(in *Mailer) (*WelcomeSender, error) {
    return &WelcomeSender{
        mailer: in,
    }, nil
}
```

You might be tempted to think that we are done. After all, we are injecting the dependency, Mailer, into WelcomeSender.
Sadly, we are not quite there yet. In fact, we are missing the real purpose of DI. No, it's not testing, although we will get to that. The real purpose of DI is decoupling.

At this point, our WelcomeSender cannot work without an instance of Mailer. They are tightly coupled. So, let's decouple them by applying the DIP.

```go
type Mailer struct {
    Host string
    Port string
    Username string
    Password string
}
func (m *Mailer) Send(to string, body string) error {
    return nil
}

func (m *Mailer) Receive(address string) (string,error) {
    return "", nil
}

// we introduce an abstraction by converting this into an interface based on the method signatures
type MailerInterface interface {
    Send(string, string) error
    Receive(string) (string, error)
}

// Hang on, we only need to send emails. Let's apply the interface segregation principle and reduce the interface to only the methods we use and update our constructor
type Sender interface {
    Send(string, string) error
}

func NewWelcomeSenderV2(in Sender) *WelcomeSenderV2 {
    return &WelcomeSenderV2{
        sender: in,
    }
} 
```

With this one small change, a few handy things have happend. Firstly, our code is now entirely self-contained. This means any bugs, extensions, tests or other changes will only involve this package. Second, we can use mocks or stubs to test our code, stopping us from spamming ourselves with emails and requiring a working email server for our tests to pass. Lastly, we are no longer tied to the Mailer class. If we wanted to change from a welcome email to an SMS or tweet, we could change our input parameter to a different Sender and be done.

By defining our dependency as an abstraction (as a local interface) and passing that dependency into our constructor, we have explicitly defined our requirements and given us greater freedom in our testing and extensions.

## Addressing the duck in the room

What is duck typing?

If it looks like a duck, and it quacks like a duck, then it is a duck
Or, put more technically:
At runtime, dynamically determine an object's suitability based only on the parts of that object that are accessed

Let's look at a Go example to see if it supports duck typing:

```go
type Talker interface {
    Speak() string
    Shout() string
}

type Dog struct {}
func (d Dog) Speak() string {
    return "Woof!"
}
func (d Dog) Shout() string {
    return "WOOF!"
}
func SpeakExample() {
    var talker Talker
    talker = Dog{}
    fmt.Print(talker.Speak())
}
```

As you can see, our Dog type does not declare that it implements the Talker interface, as we might expect from Java or C#, and yet we are able to use it as a Talker.

From our example, it looks like Go might support duck typing, but there are a couple of problems:

- In duck typing, compatibility is determined at runtime; Go will check our Dog type implements Talker at compile Time.
- In duck typing, suitability is only based on the parts of the object accessed. In the previous example, only the Speak() method is actually used. However, if our Dog type did not implement the Shout() method, then it would fail to compile.

## Advantages of constructor injection

**Separation from the dependency life cycle**

**Easy to implement**

**Predictable and concise**

**Encapsulation**

**Helps to uncover code smells**
It's an easy trap to add just one more feature to an existing struct or interface. As we saw during our earlier discussions of the single responsibility principle, we should resist this urge and keep our objects and interfaces as small as possible. One easy way to spot when an object has too many responsibilities is to count its dependencies. Typically, the more responsibilities an object has, the more dependencies it will accumulate.

```go
// RegisterHandler is the HTTP handler for the Register endpoint
type RegisterHandler struct {
}

// ServeHTTP implements http.Handler
func (h *RegisterHandler) ServeHTTP(resp http.ResponseWriter, req *http.Request) {
    // extract payload from request
    requestPayload, err := h.extractPayload(request)
    if err != nil {
        // output error
        resp.WriteHeader(http.StatusBadRequest)
        return
    }
    // register person
    id, err := h.register(requestPayload)
    if err != nil {
        response.WriteHeader(http.StatusBadRequest)
        return
    }
    // happy path
    resp.Header().Add("Location", fmt.Sprintf("/person/%d/", id))
    resp.WriteHeader(http.StatusCreated)
}
```
