# Monkey patching

Monkey patching is changing a program at runtime, typically by replacing a function or variable. Monkey patching can be used to test in ways that are otherwise impossible.

## Monkey magic
