# The Dependency Graph

What would a perfect graph look like? If there were one, it would be very flat, with pretty much everything hanging from the main package. In such a system, all of the packages would be completely decoupled from each other and would have no dependencies beyon their external dependencies and the standard library.

The config package, just about every package depends on it. As we have seen, with this amount of responsibility, making changes to that package is potentially tricky. Not far behind in terms of trickiness is the logging package.