# Introduction to the ACME Registration Service

## Goals for our system

How do we define a healthy system? The system we have works; it does what the business needs it to do. That's enough, right?

A healthy code base has the following key features:

• High readability
• High testability
• Low coupling

### Low coupling

Coupling is a measure of how an object or package relates to others. An object is considered to have high coupling if changes to it will likely result in changes to other object, or vice versa. Conversely, when an object has low coupling, it is independent of other objects or packages. In Go, low coupling is best achived through implicit interfaces and stable and minimal exported APIs.

Low coupling is desirable as it leads to code where changes are localized. In the following example, by using an implicit interface to define our requirements we are able to insulate ourselves from changes to our dependency.

```go
// package achiver
// 
// achiver package no longer depend on the file package
// this lack of dependence also means that we have less context to remember when reading the code and fewer dependencies when writing our tests.
type Uploader interface {
    Upload(string, []byte) error
}

type Archiver struct {
    Compress(Uploader, string, []byte) error
}

// package file
type FileManager interface {
    Upload(string, []byte) error
    Download(string) ([]byte, error)
} 
```

## Introduction to our system

• REST: This package accepts the HTTP requests and converts them into function calls in the business logic. It then converts the business logic response back into HTTP.
• Business logic: This is where the magic happens. This layer uses the external service and data layer to perform the business functions.
• External Services and Data: This layer consists of code that accesses the database and the upstream services that provides the currency exchanges rates.

## Testability

```go
func TestGetHandler_ServeHTTP(t *testing.T) {
    // ensure the test always fails by giving it a timeout
    ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
    defer cancel()
    // create and start a server 
    // without current implementation, we cannot test this handler
    // without a full server as we need the mux
    address, err := startServer(mux)
    require.NoError(t, err)
    // build inputs
    response, err := http.Get("http://" + address + "/person/1/")
    // validate outputs
    require.NoError(t, err)
    require.Equal(t, http.StatusOK, response.StatusCode)
    expectedPayload := []byte(`{"id": 1, "name": "John", "phone": "01234", "currency": "USD", "price": 100}` + "\n")
    payload, _ := ioutil.ReadAll(response.Body)
    defer response.Body.Close()
    assert.Equal(t, expectPayload, payload)
}
```

If the URL path changed, this test would break
If the output format changed, this test would break
If the config file wasn't configured correctly, this test would break
If the database wasn't working, this test would break
If the record ID 1 were missing from the database, this test would break
If the business logic layer had a bug, this test would break
If the database layer had a bug, this test would break

## Duplication of effort

```go
func TestGetter_Do(t *testing.T) {
    // inputs
    ID := 1
    name := "John"
    // call method
    getter := &Getter{}
    person, err := getter.Do(ID)

    require.NoError(t, err)
    assert.Equal(t, ID, person.ID)
    assert.Equal(t, name, person.FullName)
}
```
