# Liskov substitution principle

Subtypes must be substitutable for their base types.

Go doesn't have abstract classes or inheritance, it does have a composition and interface implementation.

**LSP refers to behavior and not implementation**

An object can implement any interface that it likes, but that doesn't make it behaviorally consistent with other implementations of the same interface.

```go
type collection interface {
	add(item interface{})
	get(index int) interface{}
}

type collectionImpl struct {
	items []interface{}
}

func (c *collectionImpl) add(item interface{}) {
	c.items = append(c.items, item)
}

func (c *collectionImpl) get(index int) interface{} {
	return c.items[index]
}

type readonlyCollection struct {
	collectionImpl
}

func (ro *readonlyCollection) add(item interface{}) {
	ro.collectionImpl.add(item)
}
```

What happends when you have a function that accepts a Collection? When you call Add(), what would you expect to happen?

The fix, in this case, might surprise you. Instead of making an ImmutableCollection out of a MutableCollection, we can flip the relation over, as shown in the following code:

```go
type ImmutableCollection interface {
	Get(index int) interface{}
}

type MutableCollection interface {
	ImmutableCollection
	Add(item interface{})
}

type readonlyCollection struct {
	items []interface{}
}

func (ro *readonlyCollection) Get(index int) interface{} {
	return ro.items[index]
}

type collectionImpl struct {
	readonlyCollection
}

func (c *collectionImpl) Add(item interface{}) {
	c.items = append(c.items, item)
}
```

A bonus of this new structure is that we can now let the compiler ensure that we don't use ImmutableCollection where we need MutableCollection

## How does this relate to DI?

By following LSP, our code performs consistently regardless of the dependencies we are injecting. Violating LSP, on the other hand, leads us to violate OCP. The violations cause our code to have too much knowledge of the implementations, which in turn breaks the abstraction of the injected dependencies.

## What does this mean for Go?

When using composition-particularly the unnamed variable form-to satisfy interfaces. When implementing interfaces, we can use LSP's focus on consistent behavior as a way of detecting code smell related to incorrect abstractions.
