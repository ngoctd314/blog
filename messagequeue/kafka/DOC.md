# Documentation

## Getting Started

### Introduction

**What is event streaming?**

Technically speaking, event streaming is the practice of capturing data in real-time from event sources like databases, cloud, sensors..., and software applications in the processing and reacting to the event streams in real-time; and routing the event streams to different destination technologies as needed. Event streaming thus ensures a continuous flow and interpretation of data so that the right information is at the right place, at the right time.

**What can I use event streaming for?**

- To servce as the foundation for data platforms, event-driven architectures, and microservices.

**Apache Kafka is an event streaming platform. What does that mean?**

1. To publish (write) and subscribe to (read) streams of events, including continuous import/export of your data from other system.
2. To store streams of events durably and reliably for as long as you want
3. To process streams of events as they occur or retrospectively

ALl this functionality is provided in a distributed, highly scalable, elastic, fault-tolerant and secure manner.

**How does Kafka work in a nutshell?**

Kafka is a distributed system consisting of servers and clients that communicate via a high-performance TCP network protocol.

**Servers:** Kafka is run as a cluster of one or more servers that can span multiple datacenters or cloud regions. Some of these servers form the storage layer called the brokers. A kafke cluster is highly scalable and fault-tolerant: if any of its servers fails, the other servers will take over their work to ensure continuous operations without any data loss.

**Clients:** They allow you to write distributed applications and microservices that read, write and process streams of events in parallel, at scale and in a fault-tolerant manner even in the case of network problems or machine failures.

#### Main Concepts and Terminology

**An event records**

The fact that "something happend" in the word or in your business. When you read or write data to Kafka, you do this in the form of events. Conceptually, an event has a key, value, timestamp and optional metadata headers.

```txt
- Event key: "Alice"
- Event timestamp: "Jun. 25, 2020 at 2.06 p.m"
- Event value: "Made a payment of $200 to Bob"
```

Event are organized and durably stored in topics. Very simplified, a topic is similar to a folder in a filesystem, and the events are the files in that folder. Topics in Kafka are always multi-producer and multi-subscribers: a topic can have zero, one or many producers that write events to it, as well as zero, one or many consumers that subscribe to these events. Events in a topic can be read as often as needed - unlike traditional messaging systems, events are not deleted after comsumption. Instead you define for how long Kafka should retain your events through a per-topic configuration setting, after which old events will be discarded.

Topics are partitioned, meaning a topic is speard over a number of "buckets" located on different Kafka brokers. This distributed placement of your data is very important for scalability because it allows client applications to both read and write the data from/to many brokers at the same time. When a new event is published to a topic, it is actually appended to one of the topic's partitions. Events will the same events key are written to the same partition and Kafka guarantees that any consumer of a given topic-partition will always read that partition's events in exactly the same order as the were written.

To make your data fault-tolerant and highly-avaiable, every topic can be replicated

**Producers**

Producers are those client applications that publish (write) events to Kafka

**Consumers**

Consumers are those that subscribe to (read and process) these events.

**Producers and consumers**

In Kafka, producers and consumers are fully decoupled and agnostic of each other, which is a key design to echieve high scalability that Kafka is known for. Producers never need to wait for consumers.

**Partitions**

Kafka's topics are divided into several partitions. While the topic is a logical concept in Kafka, a partition is the smallest storage unit that holds a subset of records owned by a topic. Each partition is a single log file where records are written to it in an append-only fashion.

The records in the paritions are each assigned a sequential identifier called the offset, which is unique for each record within the partition.

The offset is an incremental and immutable number, maintained by Kafka. When a record is written io a parition, it is appended to the end of the log, assigning the next sequential offset. Offsets are particularly useful for consumers when reading records from a partition. We'll come to that at a later point.

Kafka distributes the partitions of a particular topic across multiple brokers. By doing so, we'll get the following benefits.

- If we are to put all paritions of a topic in a single brocker, the scalability of that topic will be constrained by the broker's IO throughput. A topic will never get bigger than the biggest machine in the cluster. By speading paritions across multiple brokers, a sinlge topic can scaled horizontally to provide performance far beyond a single broker's ability

- A single topic can be consumed by multiple consumers in parallel. Serving all partitions from a single broker limits the number of comsumers it can support. Partitions on multiple brokers enable more consumer

- Multiple instance of the same consumer can connect to partitions on different brokers.

### Use Cases

**Messaging**
**Website Activity Tracking**
**Metrics**
**Log Aggregation**
**Stream Processing**
**Event Sourcing**
**Commit Log**
