# Kafka in Action

## Part I

- Apache Kafka is a streaming platform that you can leverage to process large numbers of events quickly

## Part II. Applying Kafka

**Producing and consuming a message**

A message, also called a record is the basic piece of data flowing through Kafka. Each message has a timestamp, a value and an optional key.

**What are brokers?**

**Kafka Architecture**

|Component|Role|
|---|---|
|Producer|Sends messages to Kafka|
|Consumer|Retrieves messages from Kafka|
|Topics|Logical name of where messages are stored in the broker|
|ZooKeeper ensemble|Helps maintain consensus in the cluster|
|Broker|Handles the commit log(how messages are stored on the disk)|

**Producers and consumers**
