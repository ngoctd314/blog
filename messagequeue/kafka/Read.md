# Reading records from partitions

[via](https://medium.com/event-driven-utopia/understanding-kafka-topic-partitions-ae40f80552e8#:~:text=Partitions%20are%20the%20way%20that%20Kafka%20provides%20redundancy.,partitions%20that%20failed%20broker%20owned.)

Unlike the other pub/sub implementations, Kafka doesn't push messages to consumers. Instead, consumers have to pull messages off Kafka topic partitions. A consumer connects to a partition in a broker, reads the messages in the order in which they were written.

The offset of a message works as a consumer side cursor at this point. The consumber keeps track of which messages it has already consumed by keeping track of the offset of messages. After reading a message, the consumer advances its cursor to the next offset in the partition and continues. Advancing and remembering the last read offset within a partition is the responsibility of the consumer. Kafka has nothing to do with it.

Each consumer has its own view about the partition
A partition can be consummed by one or more consumers, each reading at different offsets.

Kafka has the concept of consumer groups where several consumers are grouped to consume a given topic. Consumers in the same consumer group are assigned the same group-id value.

The consumer group concept ensures that a message is only ever read by a single consumer in the group. When a consumer group consumes the partition of a topic, Kafka makes sure that each partition is consumed by exactly one consumer in the group.

Consumer groups enable consumers to parallelize and procerss messages at very high throughputs. However, the maximum parallelism of a group will be equal to the number of partitions of that topic.

For example, if you have N + 1 consumers for a topic with N partitions, then the first N consumers will be assigned a partition, and the remaining consumer will be idle until another consumer fail.
