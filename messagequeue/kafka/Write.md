# Writing records to paritions

## Using a parition key to specify the partition

A producer can use a partition key to direct messages to a specific partition. A partition key can be any value that can be derived from the application context. A unique device ID or user ID will make a good partition key.

By default, the partition key is passed through a hashing function, which creates the partition assignment. That assures that all records produced with the same key will arrive at the same partition. Specifying a partition key enables keeping related events together in the same partition and in the exact order in which they were sent.

Messages with the same partition key will end up at the same partition

Key based partition assignment can lead to broker skew if keys aren't well distributed.

For example, when customer ID is used as the partition key, and one customer generates 90% traffic, then one partition will be getting 90% of the traffic most of the time. On small topics, this is negligible, on larger ones, it can sometime take a broker down.

When choosing a partition key, ensure that they are well distributed.

## Allowing Kafka to decide the partition

If a producer doesn't specify a partition key when producing a record, Kafka will use a round-robin partition assignment. Those records will be written evenly across all partitions of a particular topic. 

However, if no partition key is used, the ordering of records can not be guaranteed within a given partition.

## Writing a custom partitioner

In some situations, a producer can use its own partitioner implementation that uses other business rules to do the partition assignment.
