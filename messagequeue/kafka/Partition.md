# Partitions

Kafka's topics are divided into several paritions. While the topic is a logical concept in Kafka, a partition is the smallest storage unit that holds a subset of records owned by a topic. Each partition is a single log file where records are written to it in an append-only fashion.

## Offsets and the ordering of messages

The records in the partitions are each assigned a sequential identifier called the offset, which is unique for each record within the partition

The offset is an incremental and immutable number, maintained by Kafka. When a record is written to a partition, it is appended to the end of the log, assigning the next sequential offset. Offsets are particularly useful for consumers when reading records from a partition.

## Partitions are the way that Kafka provides scalability

A Kafka cluster is made of one or more servers. In the Kafka universe, they are called Brokers. Each broker holds a subset of records that belongs to the entire cluster.

Kafka distributes the partitions of a particular topic across multiple brokers.

- If we are to put all partitions of a topic in a single broker, the scalability of that topic will be constrained by the broker's IO throughput. A topic will never get bigger than the biggest machine in the cluster. By spreading partitions across multiple brokers, a single topic can be scaled horizontally to provide performance far beyond a single broker's ability.

- A single topic can be consumed by multiple consumers in parallel. Serving all paritions from a single broker limits the number of consumers it can support. Partitions on multiple brokers enable more consumers.

- Multiple instances of the same consumer can connect to partitions on different brokers, allowing very high message processing throughput. Each consumer instance will be served by one partition, ensuring that each record has a clear processing owner.

## Paritiona are the way that Kafka  provides redundancy

Kafka keeps more than one copy of the same partition across multiple brokers. This redundant copy is called a replica. If a broker fails, Kafka can still serve consumers with the replicas of partitions that failed broker owned.

## Kafka Topic Partition Replication

Kafka can replicate partitions across a configurable number of Kafka servers which is used for tolerance. Each partition has a leader server and zero or more follower servers. Leaders handle all read and write request for a partition. Followers replicate leaders and take over if the leader dies.

## Replication: Kafka Partition Leaders, Followers and ISRs

Kafka chooses one broker's partition's replicas as leader using ZooKeeper
The broker that has the partition leader handles all reads and writes of record for the partition. Kafka replicates writes to the leader partition to followers.

## Kafka Replication to Partition 0

Record is considered "committed" when all ISRs for partition wrote to their log.
