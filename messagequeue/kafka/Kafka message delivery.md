# Kafka message delivery

Kafka's message delivery can take at least the following three delivery methods

- At-least-once semantics: A message is sent as needed until it is acknowledged

If a message from a producer has a failure or is not acknowledged, **the producer resends the message.**
The broker sees two messages at least once (or only one if there is a failure).
Consumers get as many messages as the broker receives. Consumers might see duplicate messages.

- At-most-once semantics: A message is only sent once and not resent on failure

If a message from a producer has a failure or is not acknowledged, **the producer does not resend the message.**
The broker sees one message at most (or zero if there is a failure).
Consumers see the messages that the broker receives. If there is a failure, the consumer never sees that messages.

- Exactly-once semantics: A message is only seen once by the consumer of the message

If a message from a producer has a failure or is not acknowledged, the producer resends the message.
The broker only allows one message
Consumers only see the message once
