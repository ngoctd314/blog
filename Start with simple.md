# Start with simple - get complicated only when you must 

```go
func NotNoSimple(ID int64, name string, age int, registered bool) string {
	out := &bytes.Buffer{}
	out.WriteString(strconv.FormatInt(ID, 10))
	out.WriteString("-")
	out.WriteString(strings.Replace(name, " ", "_", -1))
	out.WriteString("-")
	out.WriteString(strconv.Itoa(age))
	out.WriteString("-")
	out.WriteString(strconv.FormatBool(registered))
	return out.String()
}
```

```go
func Simpler(ID int64, name string, age int, registered bool) string {
    nameWithNoSpaces := strings.Replace(name, " ", "-", -1)
    return fmt.Sprintf("%d-%s-%d-%t", ID, nameWithNoSpaces, age, registered)
}
```

In the first code, an entire system will almost certainly make it run faster, but not only did it likely take longer to code, but it's also harder to read and therefore maintain and extend.

## Apply just enough abstraction

Excessive abstraction leads to an excessive mental burden and excessive typing.

```go
type myGetter interface {
    Get(url string) (*http.Response, error)
}

func TooAbstract(getter myGetter, url string) ([]byte, error) {
    resp, err := getter.Get(url)
    if err != nil {
        return nil, err
    }
}
```

Compare with

```go
func CommonConcept(url string) ([]byte, error) {
    resp, err := http.Get(url)
    if err != nil {
        return nil, err
    }
}
```

## Export only what you must

