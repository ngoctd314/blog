# SRP refactor

Long method without SRP

```go
// method knows too much, it makes me know to much
// The input format could change
// The database format could change
// The business rules could change
func longMethod(resp http.ResponseWriter, req *http.Request) {
	err := req.ParseForm()
	if err != nil {
		resp.WriteHeader(http.StatusPreconditionFailed)
		return
	}

	userID, err := strconv.ParseInt(req.Form.Get("UserID"), 10, 64)
	if err != nil {
		resp.WriteHeader(http.StatusBadRequest)
		return
	}

	row := DB.QueryRow("SELECT * FROM Users WHERE userID = ?", userID)
	person := &Person{}
	err := row.Scan(person.ID, person.Name, person.Phone)
	if err != nil {
		resp.WriteHeader(http.StatusInternalServerError)
		return
	}
	encoder := json.NewEncoder(resp)
	err = encoder.Encode(person)
	if err != nil {
		resp.WriteHeader(http.StatusInternalServerError)
		return
	}
}

func TestLongMethod_happyPath(t *testing.T) {
    // build request
    request := &http.Request{}
    request.PostForm = url.Values{}
    request.PostForm.Add("UserID", "123")
    // mock the database
    var mockDB sqlmock.Sqlmock
    var err error
    DB, mockDB, err = sqlmock.New()
    require.NoError(t, err)
    mockDB.ExpectQuery("SELECT * FROM people WHERE ID = ?").WithArgs(123).WillReturnRows([]string{"ID", "Name", "Phone"}).AddRow(123, "May", "012345")

    response := httptest.NewRecorder() 
    // call method
    longMethod(response, request)
    // validate response
    require.Equal(t, http.StatusOK, response.Code)
    // validate JSON
    responseBytes, err := ioutil.ReadAll(response.Body)
    require.NoError(t, err)
    expectedJSON := `{"ID":123,"Name":"May","Phone":"0123456789"}` + "\n"
    assert.Equal(t, expectedJSON, string(responseBytes))
}
```

With SRP

```go
func shortMethods(resp http.ResponseWriter, req *http.Request) {
    userID, err := extractUserID(req)
    if err != nil {
        resp.WriteHeader(http.StatusInternalServerError)
        return
    }
    person, err := loadPerson(userID)
    if err != nil {
        resp.WriteHeader(http.StatusInternalServerError)
        return
    }
    outputPerson(resp, person)
}
```
