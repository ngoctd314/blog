# Interface refactor

```go
// interfaces and structs with fewer methods are easier to understand
type WideFormatter interface {
    ToCSV(pets []Pet) ([]byte, error)
    ToGOB(pets []Pet) ([]byte, error)
    ToJSON(pets []Pet) ([]byte, error)
}

// compare with
type Formatter interface {
    Format(pets []Pet) ([]byte, error)
}

// straightforward code, but more code
// providing a better UX for users 
type CSVFormatter struct {}
func (f CSVFormatter) Format(pets []Pet) ([]byte, error) {} 
```
