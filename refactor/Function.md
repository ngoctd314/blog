# Function refactor

```go
// legacy code
// What does true mean? It's hard to tell without opening the function or the documentation
NewPet("Fido", true)

// refactor code
// Purpose is clear, mistakes are unlikely and, as a bonus, encapsulation is improved
NewDog("Fido")
```

```go
// PetFetcher searches the data store for pets whose name matches the search string
// Limit is optional (default 100)
// Offset is optional (default 0)
// sortBy is optional (default name)
// asc is optional (default true)
func PetFetcher(search string, limit int, offset int, sortBy string, asc bool) []Pet {
    return nil
}

// That probably looks OK, right? The problem is that most of the usage looks like the following:
// many inputs are ignored
results := PetFetcher("Fido", 0, 0, true)

// Option 1: multiple function (another params with have default value)
func PetFetcherByName(search string) []Pet {
    return nil
}
func PetFetcherByNameAndLimit(search string, limit int) []Pet {
    return nil
}

// Option 2: extract to object
type fetchObj struct {
    search string
    limit int
    offset int
    sortBy string
    asc bool
}
func PetFetcher(fetch fetchObj) []Pet {
    return nil
}

// Option 3: with pattern
func PetFetcher(search string) []Pet {}
func WithLimit(pets []Pet, limit int) []Pet {}
func WithOffset(pets []Pet, offset int) []Pet {}
func WithAsc(pets []Pet, asc bool) []Pet {}

// Option 4: Map - Reducer (JS world)
```
