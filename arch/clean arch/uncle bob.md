# Clean arch

Each of these architectures produce systems that are:

1. Independent of Frameworks. The architecture does not depend on the existence of some library. This allows you to use such frameworks as tools, rather than having to cram your system into their limited constraints.

2. Testable. The business rules can be tested without the UI, Database, Web Server, or any other external element.

3. Independent of UI.

4. Independent of Database.

5. Independent of any external agency.

**The Dependency Rule**

The concentric circles represent different areas of software. In general, the further in you go, the higher level the software becomes. The outer circles are mechanisms. The inner circles are policies.

The overriding rule that makes architecture work is The Dependency Rule. This rule says that source code dependencies can only point inwards. Nothing in an innner circle can know anything at all about something in an outer circle. In particular, the name of something declared in an outer circle must not be mentioned by the code in the an inner circle.

**Entities**

Entities encapsulate Enterprise wide business rules. An entity can be an object with methods, or it can be a set of data structures and functions. It doesn't matter so long as the entities could be used by many different applications in the enterprise.

If you don't have an enterprise, and are just writing a single application, then these entities are the business objects of the application. They encapsulate the most general and high-level rules. They are least likely to change when something external changes. No operational change to any particular application should affect the entities layer.

**Use Cases**

The software in this layer contains application specific business rules. It encapsulates and implements all of the usecases of the system. These use cases orchestrate the flow of data to and from the entities, and direct those entities to use their enterprise wide bisiness rules to achieve the goals of the use case.

We don't expect changes to this layer to affect the entities. We also do not expect this layer to be affected by changes to externalities such as the database, the UI, or any of the common frameworks. This layer is isolated from such concern.

If the details of a use-case change, then some code in this layer will certainly be affected.

**Interface Adapters**
