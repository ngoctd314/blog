# UML Diagram

[link](https://uml.gitbook.io/learning-uml-with-visual-paradigm/class-diagram-tutorial)

## Class relationships

There are six main types of relationships between classes: inheritance, implementation, composition, aggregation, association, and dependency.

## Six types of relationships

In the six types of relationships such as composition, aggregation, and association is the same as using attributes to store the references of another class.

## Composition vs aggregation

**Composition**
I own an object and I am responsible for its lifetime. When Foo dies, so does bar. I think when you use composition, you can use dependency injection

```java
public class Foo {
    private Bar bar = new Bar();
}
```

**Aggregation**
I have an object which i've borrowed from someone else. When Foo dies, Bar **may** live on. I think when you use aggregation, you must use dependency injection

```java
public class Foo {
    private Bar bar;
    Foo(Bar bar) {
        this.bar = bar;
    }
}
```
