# Interface segregation principle (ISP)

Clients should not be forced to depend on methods they do not use.

Let's first discuss why fat interfaces might be a bad thing. Fat interfaces have more methods and are therefore likely to be harder to understand. They also require more work to use, whether this be through implementing, mocking so stubbing them.

Fat interfaces indicate more responsibility and, as we saw with the SRP, the more responsibility an object has, the more likely it will want to change. If the interface changes, it causes a ripple effect through all its users, violating OCP and causing a massive amount of shotgun surgery. This is the first advantage of ISP:

**ISP requires us to define thin interfaces**

For many programmers, their natural tendency is to add to the existing interface rather than define a new one, thereby creating a fat interface. This leads to a situation implementing becomes tightly coupled with the users of the interface. This coupling then makes the interface, their implementations and users all the more resistant to change.

```go
type FatDbInterface interface {
	BatchGetItem(IDs ...int) ([]Item, error)
	BatchGetItemWithContext(ctx context.Context, IDs ...int) ([]Item, error)
	BatchPutItem(items ...Item) error
	BatchPutItemWithContext(ctx context.Context, items ...Item) error
	DeleteItem(ID int) error
	DeleteItemWithContext(ctx context.Context, item Item) error
	GetItem(ID int) (Item, error)
	GetItemWithContext(ctx context.Context, ID int) (Item, error)
	PutItem(item Item) error
	PutItemWithContext(ctx context.Context, ID int) (Item, error)
	Query(query string, args ...interface{}) ([]Item, error)
	QueryWithContext(ctx context.Context, query string, args ...interface{}) ([]Item, error)
	UpdateItem(item Item) error
	UpdateItemWithContext(ctx context.Context, item Item) error
}

type Item struct{}
type Cache struct {
	db FatDbInterface
}

func (c *Cache) Get(key int) interface{} {
	c.db.GetItem(key)
	return nil
}

func (c *Cache) Set(key string, value interface{}) {
	c.db.PutItem(Item{})
}
```

It's not hard to imagine all of these methods belonging to one struct. Method pairs such as GetItem() and GetItemWithContext() are quite likely to share much, if not almost all, of the same code. On the other hand, a user of GetItem() is not likely to also use GetItemWithContext(). For this particular use case, a more appropriate interface would be the following:

```go
type Item struct{}
type myDB interface {
	GetItem(ID int) (Item, error)
	PutItem(item Item) error
}

type CacheV2 struct {
	db myDB
}

func (c *CacheV2) Get(ID int) (Item, error) {
	c.db.GetItem(ID)
	return Item{}, nil
}

func (c *CacheV2) Set(item Item) error {
	c.db.PutItem(item)
	return nil
}
```

**ISP leads to explicit inputs**

A thin interface is also more straightforward to more fully implement, keeping us away from any potential problems with LSP. Is cases where we are using an interface as an input and the interface needs to be fat, this is a powerful indication that the method is violating SRP.
