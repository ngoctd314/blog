# Designing a URL Shortening service like TinyURL

**TOC**

1. Why do we need URL shortening?
2. Requirements and Goals of the System
3. Capacity Estimation and Constraints
4. System APIs
5. Database Design

- Database Schema

6. Basic System Design and Algorithm
7. Data Partitioning and Replication
8. Cache
9. Load Balancer (LB)
10. Purging or DB cleanup
11. Telemetry
12. Security and Permissions

## Requirements and Goals of the System

**Functional Requirements**

- long URL -> short URL (ex. n / 3)
- multiple choice: long URL -> short URL 1, short URL 2, short URL 3
- custom expire time
- same endpoint: short URL redirect to long URL

**Non-Functional Requirements**

- HA, system down -> short URL down -> can't redirect to origin URL
- low latency, URL redirection should happen in real-time
- Shortened links should not be guessable (not predictable)

**Extended Requirements**

1. Analytics; how many times a redirection happend?
2. Our service should also be accessible through REST APIs by other services

## Capacity Estimination and Constraints

Our system will be read-heavy. There will be lots of redirection requests compared to new URL shortenings. Let's assume 100:1 ratio between read and write

## Database Design

Defining the DB schema in the early stages of the interview would help to understand the data flow among various components and later would guide towards data partitioning.

1. We need to store billions of records
2. Each object we store is small (less than 1K)
3. There are no relationships between records - other than storing which user created a URL
4. Our service is read-heavy

**What kind of database should we use?**
Since we anticipate storing billions of rows, and we don't need to use relationships between objects - a NoSQL store like DynamoDB, Cassandra is a better choice. A NoSQL choice would also be easier to scale.

## Basic System Design and Algorithm
