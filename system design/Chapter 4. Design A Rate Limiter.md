# Design A Rate Limiter

In a newwork system, a rate limiter is used to control the rate of traffic sent by a client or a service. In the HTTP world, a rate limiter limits the number of client requests allowed to be sent over specified period. If the API request count exceeds the threshold defined by the rate limiter, all the excess calls are blocked.

In this chapter, you are asked to design a rate limiter. Before  starting the design, we first look at the benefits of using an API rate limiter:

- Prevent resource starvation caused by Denial of Service (DoS) attack. Almost all APIs published by large tech companies enforce some form of rate limiting.
- Reduce cost. Limiting excess requests means fewer servers and allocating more resources to high priority APIs. Rate limiting is extremely important for companies that use paid third party APIs.
- Prevent servers from being overloaded. To reduce server load, a rate limiter is used to filter out excess requests caused by bots or users misbehavior.

## Understand the problem and establish design scope

Candidate: What kind of rate limiter are we going to design? It is a client-side rate limiter or server-side API rate limiter?
Interviewer: We focus on the server-side API rate limiter

Candidate: Does the limiter limit API requests based on IP, the user ID, or other properties?
Interviewer: The rate limiter should be flexible enough to support different sets of rules

Candidate: What is the scale of the system? It is build for a startup of a big company with a large user base?
Interviewer: The system must be able to handle a large number of requests

Candidate: Will the system work in a distributed env?
Interviewer: Yes

Candidate: Is the rate limiter a separate service or should it be implemented in application code?
Interviewer: It is a design decision up to you

Candidate: We need to inform users who are throttled?
Interviewer: Yes

Requirements:

Here is a summary of the requirements for the system:

- Accurately limit excessive requests
- Low latency. The rate limiter should not slow down HTTP response time.
- Use as litte memory as possible
- Distributed rate limiting. The rate limiter can be shared across multiple servers or processes.
- Exception handling. Show clear exceptions to users when their requests are throttled.
- High fault tolerance. If there are any problems with the rate limiter, it doesn't affect the entire system.

## Propose high-level design and get buy-in

Besides the client and server-side implementations, there is an alternative way. Instead of putting a rate limiter at the API servers, we create a rate limiter middeleware, which throttles requests to your APIs are shown in Figure 4.2

Cloud microservices have become widely popular and rate limiting is usually implemented within a component called API gateway. API gateway is a fully managed service that supports rate limiting, SSL, authentication, IP whitelisting, servicing static content, etc. For now, we only need to know that the API gateway is a middleware that supports rate limiting.

While designing a rate limiter, an important question to ask ourselves is: where should the rater limter be implemented, on the server-side or in a gateway?

- Evaludate your curent technology stack, cache service, etc
- Identify the rate limiting algorithm that fits your business needs. When you implement everything on the server-side, you have full control of the algorithm. However, your choice might be limited if you use a third-party gateway.
- If you have already used microservice architecture and included and API gateway in the design to perform authentication, IP whitelisting, etc, you may add a rate limiter to the API gateway.
- Building your own rate limiting service takes time. If you do not have enough engineering resources to implement a rate limiter, a commercial API gateway is a better option.

## Algorithms for rate limiting

### Token bucket algorithm

A token bucket is a container that has pre-defined capacity. Tokens are put in the bucket at preset rates periodically. Once the bucket is full, no more tokens are added. Each request consumers one token. When a request arrives, we check if there are enough tokens in the bucket.

- If there are enough tokens we take one token out for each request, and the request goes through.
- If there are not enough tokens, the request is dropped.

Example, the token bucket size is 4, and the refill rate is 4 per 1 minute.

### Leaking bucket algorithm

The leaking bucket algorithm is similar to the token bucket except that requests are processed at a fixed rate. It is usually implemented with a FIFO queue.

- The a request arrives, the system checks if the queue is full. If it is not full, the request is added to the queue

- Otherwise, the request is dropped
- Requests are pulled from the queue and processed at regular intervals

Leaking bucket algorithm takes the following two parameters:

- Bucket size: it is equal to the queue size. The queue holds the requesrts to be processed at a fixed rate
- Outflow rate: it defines how many requests can be processed at a fixed rate

### Fixed window counter algorithm

Fixed window counter algorithm works as follows:

- The algorithm divides the timeline into fix-sized time windows and assign a counter for each window.
- Each request increments the counter by one.
- Once the counter reaches the pre-defined threshold, new requests are dropped until a new time window starts.

## High-level architecture

At high leve, we need to counter to keep track of how many requests are sent from the same user, IP address, etc. If the counter is larger than the limit the request is disallowed.

Where shall we store counters? Using the database is not a good idea due to slowness of disk access. In-memory cache is chosen because it is fast and supports time-based expiration strategy.

INCR: It increases the stored counter by 1
EXPIRE: It sets a timeout for the counter. If the timeout expires, the counter is automatically deleted.

- The client sends a request to rate limiting middleware
- Rate limiting middleware fetches the counter from the corresponding bucket in Redis and chekcs if the limit is reached or not.

- If the limit is reached, the request is rejected
- If the limit is not reached, the request is sent to API servers. Meanwhile, the system increments the counter and saves it back to Redis.

## Design deep dive
