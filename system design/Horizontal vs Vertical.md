# Horizontal vs Vertical scale

## Compare

|||
|---|---|
|Horizontal|Vertical|
|Load balancing required|N/A|
|Resilient|Single point of failure|
|Network calls (RPC)|Interprocess communication|
|Data inconsistency|Consistent|
|Scales well as users increase|Hardware limit|
