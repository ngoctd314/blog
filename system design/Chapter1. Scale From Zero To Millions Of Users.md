# Scale From Zero To Millions of Users

## Vertical scaling vs horizontal scaling

Vertical scaling, referred as "scale up", means the process of adding more powser (CPU, RAM, ect) to your servers. Horizontal scaling, referred to as "scale-out", allows you to scale by adding more servers into your pool or resources.

When traffic is low, vertical scaling is a great option, and the simplicity of vertical scaling is its main advantage. Unfornately, it comes with serious limitations.

- Vertical scaling has a hard limit. It it impossible to add unilimited CPU and memory to a single server.
- Vertical scaling does not have failover and redundancy. If one server goes down, the website/app goes down with it completely.

Horizontal scaling is more desirable fro large scale applications due to the limitations of vertical scaling.

## Load balancer

A load balancer evenly distributes incoming traffic among web servers that are defined in a load-balanced set.

## Database replication

Database replication can be used in many database management systems, usually with a master/slave relicationship between the original (master) and the copies (slaves).

A master database generally only supports write operations. A slave databases gets copies of the data from the master database and only supports read operations. All the data-modifying commands like insert, delete, or update must be sent to the master database. Most applications require a much higher ratio of reads to writes; thus, the number of slave databases in a system is usually larger than the number of master databases.

Advantages of database replication:

- Better performance: In the master-slave mode, all writes and updates happen in master nodes; whereas, read operations are distributed across slave nodes. This model improves performance because it allows more queries to be processed in parallel.
- High availability: By replicating data across different locations, your website remains in operation even if a database is offline as you can access data stored in another database server.

**What if one of the databses goes offline?**

- If only one slave database is avaiable and it goes offline, read operations will be directed to the master database temporarily. As soon as the issue is found, a new slave database will replace the old one. In case multiple slave databases are avaiable, read operations are redirected to other healthy slave databases. A new database server will replace the old one.

- If the master database goes offline, a slave database will be promoted to be the new master. All the database operations will be temporarily executed on the new master database. A new slave database will replace the old one for data replication immediately.

## Cache tier

Here are a few considerations for using a cache system:

- Decide when to use cache: Consider using cache when data is read frequently but modified infrequently. Since cached data is stored in volatile memory, a cache server is not ideal for persisting data. For instance, if a cache server restarts, all the data in memory is lost.

- Expiration policy
- Consistency: This involves keeping the data store and the cache in sync. Inconsistency can happen because data-modifying operations on the data store and cache are not in a single transaction. When scaling across multiple regions, maintaining consistency between the data store and cache is challenging.
- Evection Policy: Once the cache is full, any requests to add items to the cache might cause existing items to be removed. This is called cache eviction.

## Message queue

## Logging, metrics, automation

Logging: Monitoring error logs is important because it helps to identify errors and problems in the system. You can monitor error logs at per server level or use tools to aggregate them to a centralized service for easy search and viewing.

Metrics: Collecting different types of metrics help us to gain business insights and understand the health status of the system.

- Host level metrics: CPU, Memory, disk I/O etc
- Aggregated level metrics: for example, the performance of the entire database tier, cache tier, etc
- Key business metrics: daily active users, retention, revenue, etc