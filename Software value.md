# Software value

If you give me a program that works perfectly but it impossibile to change, then it won't work when the requirements change, and I won't be able to make it work. Therefore the program will be come useless

If you give me a program that does not work but is easy to change, then i can make it work, and keep it working as requirements change. Therefore the program will remain continually useful.

You may not find this argument convincing. After all, there's no such thing as a program that is impossible to change. However, there are systems that are practically impossible to change, because the cost of change exceeds the benefit of change.
