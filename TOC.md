# TOC

**LSP - Liskov substituation principle**

**UML Diagram Notation**

**ISP - Interface segregation principle**

**DIP - Dependency inversion principle**

**DI - Dependency Injection**

- 4 advantages of DI
- Code smells that indicate you might need DI
