# Opentelemetry

If the system becomes slow how do we find the bottleneck is the problem with.

## Concepts

**OpenTelemetry provides you with:**

- A single, vendor-agnostic instrumentation library per language with support for both automatic and manual instrumentation
- A single collector binary that can be deployed in a variety of ways including as an agent or gateway.
- An end-to-end implementation to generate, emit, collect, process and export telemetry data.
- Full control of your data with the ability to send data to multiple destinations in parallel through configuration.

**Data sources**

OpenTelemetry supports multiple data sources as defined below. More data sources may be added in the future.

### Traces

Traces track the progression of a single request, called a trace. Each unit of work in a trace is called a span; a trace is a tree of spans. Spans are objects that represent the work being done by individual services or components involved in a request as it flows through a system. A span contains a span context, which is a set of globally unique identifiers that represent the unique request that each span is a part of.

A trace contains a single root span which encapsulates the end-to-end latency for the entire request. A trace is comprised of the single root span and any number of child spans. Each span contains metadata about the operation, such as its name, start and timestamps, attributes, events and status.

**Span life cycle**

- A request is received by a service. The span context is extracted from the request headers, if it exists.
- A new span is created as a child of the extracted span context; if none exists, a new root span is created.
- The service handles the request. Additional attributes and events are added to the span that are useful for understanding the context of the request, such as the host name of the machine handling the request, or customer identifiers.
- New spans may be created to represent work being done by sub-components of the service.
- When the service makes a remote call to another service, the current span context is serialized and forwared to the next service by injecting the span context into the headers or message envelope.

### Metrics

### Logs

### Baggage

## Data Collection

**Deployment**

The OpenTelemetry Collector provides s single binary and two deployment methods:

- Agent: A collector instance running with the application or on the same host as the application
- Gateway: One or more Collector instances running as a standalone service typically per cluster, datacenter or region.
