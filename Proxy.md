# Proxy Design Pattern

## Intent

Proxy is a structural design pattern that lets you provide a substitude or placeholder for another object. A proxy controls access to the original object, allowing you to perform something either before or after the request gets through to the original object.

## Solution

The proxy pattern suggests that you create a new proxy class with the same interface as an original service object. Then you update your app so that it passes the proxy object to all of the original object's clients. Upon receiving a request from a client, the proxy creates a real service object and delegates all the work to it.

Example: The proxy disguises itself as a database object. It can handle lazy initialization and result caching without the client or the real database object even knowing.

But what's the benefit? If you need to execute something either before or after the primary logic of the class, the proxy lets you do this without changing that class. Since the proxy implements the same interface as the original class, it can be passed to any client that expects a real service object.

## Real-World Analogy

nginx web server

## Structure

1. The **Service Interface** declares the interface of the service. The proxy must follow this interface to be able to disguise itself as a service object.

2. The **Service** is a class that provides some useful business logic.

3. The **Proxy** class has a referenece field that points to a service object. After the proxy finishes its processing (lazy initialization, logging, access control, caching, etc.), it passes the request to the service object.

Usually, proxies manage the full lifecycle of their service objects.

4. The **Client** should work with both services and proxies via the same interface. This way you can pass a proxy into any code that expects a service object.

## Applicability

**Lazy initialization (virtual proxy)**
This is when you have a heavyweight service object that wastes system resources by being always up, even though you only need it from time to time.

Instead of creating the object when the app launches, you can delay the object's initialization to a time when it's really needed.

**Access control (protect proxy)**
This is when you want only specific clients to be able to use the service object.

The proxy can pass the request to the service object only if the client's credentials match some criteria.

**Local execution of a remote service (remote proxy)**
This is when the service object is located on a remote server.

In this case, the proxy passes the client request over the network, handling all of the nasty details of working with the network.

...

## How to implement

1. If there's no pre-exiting service interface, create one to make proxy and service objects interchangeable. Extracting the interface from the service class isn't always possible, because you'd need to change all of the service's clients to use that interface. Plan B is to make the proxy a subclass. Plan B is to make the proxy a subclass of the service class, and this way it'll inherit the interface of the service.

2. Create the proxy class. If should have a field for storing a reference to the service. Usually, proxies create and manage the whole life cycle of their services. On rare occasions, a service is passed to the proxy via a constructor by the client

3. Implement the proxy methods according to their purposes. In most cases, after doing some work, the proxy should delegate the work to the service object.

4. Consider introducing a creation method that decides whether the client gets a proxy or a real service.

5. Consider implementing lazy initialization for the service object.

## Pros and Cons

**Pros**

- You can control the service object without clients knowing about it.
- You can manage the lifecycle of the service object when clients don't care about it.
- The proxy works even if the service object isn't ready or is not available
- Opend/closed Principle. You can introduce new proxies without changing the service of clients.

**Cons**

- The code may become more complicated since you need to introduce a lot of new classes.
- The response from the service might get delayed
