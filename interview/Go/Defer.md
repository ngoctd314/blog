# Defer interview question

**Result and explain**

```go
func fn() ( s int ) {
    defer func() {
        s += 10
    }()
    return s
}
func main() {
    fmt.Println(fn())
}
```

Result: 10
Explain: Defer function can modify the named return results of nesting function
Defer statements are executed before the functions value is returned. In this case, the named return value i is incremented once the function completes, but before the value is returned to the calling statement in the main function.

**Moment of the Arguments of Deferred call**

The invocation moment is the moment when it is pushed into the defer call stack of its caller goroutine.
