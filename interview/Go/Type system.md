# Type system interview question

**What is purpose of type alias?**
Using aliases allows you to introduce new name for an existing type without breaking the code that references the old name.

**Diff type convension vs type assertion**
https://www.sohamkamani.com/golang/type-assertions-vs-type-conversions/

**How can an underlying type be traced given a user declared type?**
The rule is, when a build-in basic type or a non-defined type is met, the tracing should be stopped.

```go
// The underlying types of the follwing ones are both int
type (
    MyInt int // traces: int
    Age MyInt // traces: MyInt (defined type) -> int
)

// The following new types have different underlying types
type (
    IntSlice []int // underlying type is []int
    MyIntSlice []MyInt // underlying type is []MyInt
    AgeSlice []Age // underlying type is []Age
)

type Ages AgeSlice // underlying type is []Age
```
