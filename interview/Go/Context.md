# Context interview question

**Is this code safe to run?**

```go
func doSomething() {
    ctx, cancel := context.WithCancel(ctx)
    defer cancel()

    sameArg := "loremipsum"
    go dosomethingElse(ctx, someArg)
}
```

This code is not safe to run!

When we execute doSomething, we create a context and defer its cancellation, which means the context will cancel after dosomething finishes execution.
