# Solid interview in Golang

## How does the single responsibility principle improve Go code?

SRP reduces the complexity by decomposing code into smaller, more concise pieces
SRP increases the potential reusability of code
SRP makes tests simpler to write and maintain

## How does open/closed principle improve Go code?

OCP helps reduce the risk of additions and extensions
OCP can help reduce the number of changes needed to add or remove a feature
OCP narrows the locality of bugs to only the new code and its usage

## How does liskov substitution principle improve Go code?

## How does the interface segregation principle improve Go code?

ISP leads to explicit inputs

## How does the dependency inversion principle improve Go code?

## How is dependency inversion different from dependency injection
