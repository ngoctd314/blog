# Pointer interview question

**Pass value to function argument or method call is copy, then pointer is?**

Both pass args to function and method are copy, copy value and copy address

```go
func main() {
	var a = 3
	p := &a
	double(p)
	fmt.Println(p == nil, *p) // false, 6
    // false because assign copy address to nil
    // old address will continue exit and can accessible
}

func double(a *int) {
	*a += *a
    a = nil
}
```
