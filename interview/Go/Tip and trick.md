# Tips and Trick in Golang

**make sure noCopy object**

How we might detect accidental copies of types that shouldn't be copyable.
The most common way to create types that cannot be copied is to embed a sync.Mutex, and those types are already handled by the cmd/vet check

```go
type objWithoutCopy struct {
	l sync.Mutex
}

func main() {
	m := objWithoutCopy{}
	m2 := m
	fmt.Println(m2)
}
```

This is a ready solution, although it consumes memory (sync.Mutex is not a zero-size struct).

Go vet checks for the sync.Locker interface

```go
type Locker interface {
    Lock()
    Unlock()
}
```

So if we create a noCopy type that implements sync.Locker (more precisely its pointer type), that will work:

```go
type noCopy struct{}

func (*noCopy) Lock() {}
func (*noCopy) Unlock() {}

type objWithoutCopy struct {
    _ noCopy
}
```
