# Test interview question

## A security blanket named unit tests

Unit tests give you the freedom and confidence to refactor
Existing unit tests make adding new features easier
Unit tests document your intent
Unit tests document your requirements from a dependency

• Why is the usability of code important?

• Who benefits the most from code with great UX?

• How do you construct a good UX?

• What can unit testing do for you?

• What kind of test scenarios should you consider?

• How do table-driven tests help?

• How can testing damage your software design?
