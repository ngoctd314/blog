# Creational design pattern interview

## Abstract factory

**Concept**

Abstract factory is creational design pattern that let you product families of related objects without specifying their concrete classes.

**What is abstract factory interface**

Abstract factory is an interface with a list of creation methods for all products that are part of the product family. Each method must return a specify abstract product

**What is abstract product**

Abstract product is an interface with a list of necessary methods for that product
