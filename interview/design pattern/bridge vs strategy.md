# Different between bridge vs strategy pattern

[via](https://stackoverflow.com/questions/464524/what-is-the-difference-between-the-bridge-pattern-and-the-strategy-pattern#:~:text=Strategy%20Pattern%20is%20used%20for,concerned%20making%20algorithms%20more%20interchangeable.)

The Bridge pattern is a structural pattern (HOW DO YOU BUILD A SOFTWARE COMPONENT)
The Strategy pattern is a dynamic pattern (HOW DO YOU WANT TO RUN A BEHAVIOR IN SOFTWARE)

The syntax is similar but the goals are different:

- Strategy: you have more ways for doing an operation; with strategy, you can choose the algorithm at runtime and you can modify a single Strategy without a lot of side-effects at compile-time

- Bridge: you can split the hierarchy of interface and class, join it with an abstract reference

```txt
The UML class diagram for the Strategy pattern is the same as the diagram for the Bridge pattern. However, these two design patterns aren't the same in their intent. While the Strategy pattern is meant for behavior, the Bridge pattern is meant for structure.
```
