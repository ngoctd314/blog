# Common interview for design pattern

**Which two situations would a design pattern best be used in?**

- Solving a common software design problem that may been encountered before
- Fixing "spaghetti code" -- fox example, source code that has no structure or tangled program
