# Different between an API and SDK

## What is an SDK, Exactly?

SDK stands for software development kit or devkit for short. It's a set of software tools and programs used by developers to create applications for specific platforms.

SDK tools will include a range of things, including libraries, documentation, code samples, processes, and guides that developers can use and integrate into their own apps. SDKs are designed to be used for specific platforms or programing languages.

You would need an Android SDK toolkit to build an Android app, an IOS SDK to build an IOS app...
