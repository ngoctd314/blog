# Flyweight Design Pattern

Flyweight is a structural design pattern that lets you fit more objects into the available amout of RAM by sharing common parts of state between multiple objects instead of keeping all of the data in each object.

## Problem

To have some fun after long working hours, you decided to create a simple video game: players would be moving around a map and shooting each other. You chose to implement a realistic particle system and make it a distinctive feature of the game. Vast quantities of bullets, missiles and shrapnel from explosions should fly all over the map and deliver a thrilling experience to the player.

Upon its completion, you pushed the last commit, build the game and sent it to your friend for a test drive. Although the game was running flawlessly on your machine, your field wasn't able to play for long. On this computer, the game kept crashing after a few minutes of gameplay. After spending several hours digging throug debug logs, you discovered that the game crashed because of an insufficient amount of RAM. It turned out that your friend's rig was much less powerful than your own computer, and that's why the problem emerged so quickly on his machine.

The actual problem was related to your particle system. Each particle, such as bullet, a missile or a piece of shrapnel was represented by a separate object containing plenty of data. In a particular of time, newly created particles no long fit into the remaining RAM, so the program crashed.

## Solution
