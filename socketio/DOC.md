# Introduction

Socket.IO is a library that enables low-latency, bidirectional and event-based communication between a client and a server.

## What Socket.IO is not

Socket.IO is NOT a WebSocket implementation. Although Socket.IO indeed uses WebSocket for transport when possible, it adds additional metadata to each packet. That is why a WebSocket client will not be able to successfully connect to a Socket.IO server and a Socket.IO client will not be able to connect to a plain WebSocket server either.

Socket.IO is not meant to be used in a background service for mobile applications. The Socket.IO library keeps an open TCP connection to the server, which may result in a high battery drain for your users. Please use a dedicated messaging platform like FCM for this use case.

## Features

### HTTP long-polling fallback

The connection will fall back to HTTP long-polling in case the WebSocket connection cannot be established.

### Automatic reconnection

Under some particular conditions, the WS connection betwen server and client can be interrupted both sides being unaware of the broken.

That's why Socket.IO includes a hearbeat mechnism, which periodically checks status of the connection.

And when the client eventually gets disconnected, it automatically reconnects with an expontential back-off delay, in order not to overwhelm the server.
