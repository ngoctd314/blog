package observer

type subject interface {
	register(o observer)
	deregister(o observer)
	notifyAll()
}
