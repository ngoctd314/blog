# Abstract Factory

**Abstract Factory** is a creational design pattern that lets you produce families of related objects without specifying their concrete classes.

You need a way to create individual furniture objects so that they match other objects of the same family. Customers get quite mad when they receive non-matching furniture.

Also, you don't want to change existing code when adding new products or families of products to the program. Furniture vendors update their catalogs very often, and you wouldn't want to change the core code each time it happens.

## Solution

The first thing the Abstract Factory pattern suggests to explicitly declare interfaces for each distinct product of the product family. Then you can make all variants of products follow those interfaces.

All variants of the same object must be moved to a single class hierarchy (all variant must implement same interface).

The next move is to declare the Abstract Factory - an interface with a list of creation methods for all products that are part of the product family. Creation method of factory must return interface.

The client code has to work with both factories and products via their respective abstract interfaces. This lets you change the type of a factory that you pass to the client code, as well as the product variant that the client code receives, without breaking the actual client code.

Say the client wants a factory to product a chair. The client doesn't have to be aware of the factory's class, nor does it matter what kind of chair it gets. Where it's a Modern model or a Victorian-style chair, the client must treat all chairs in the same manner, using the abstract Chair interface. With this approach, the only thing that the client knows about chair is that it implements the sitOn method is some way. Also, whichever variant of the chair is returned.

There's one more thing left to clarify: if the client is only exposed to the abstract interfaces, what creates the actual factory objects? Usually, the application creates a concrete factory object at the initialization state. Just before that, the app must select the factory type depending on the configuration or the environment settings.

## Structure

**Abstract Products** declare interfaces for a set of distinct but related products which make up a product family

**Concrete Products** are various implementations of abstract products, grouped by variants (interface). Each abstract product must be implemented in all given variants

**Abstract Factory** interface declares a set of methods for creating each of the abstract products.

**Concrete Factories** implement creation methods of the abstract factory. Each concrete factory corresponds to a specific variant of products and creates only those product variants (return interface).

Although concrete factories instanitate concrete products, signatures of their creation methods must return corresponding abstract products. This way the client code that uses a factory doesn't get coupled to the specific variant of the product it gets from a factory. The Client can work with any concrete factory/product variant, as long as it communicates with their objects via abstract interfaces.

## Application

Use the Abstract Factory when your code needs to work various families of related products, but you don't want it to depend on the concrete classes of those products - they might be unkonw beforehand or you simply want to allow for future extensibility.

The Abstract Factory provides you with an interface for creating objects from each class of the product family. As long as your code creates objects via this interface, you don't have to worry about creating the wrong variant of a product which doesn't match the products creating by your app.

- Consider implementing the Abstract Factory when you have a class with a set of Factory Methods that blur its primary responsibilty

- In a well designed program each class is responsible only for one thing. When a class deals with multiple product types, it may be worth extracting its factory methods into a stand-alone factory class or a full-blown Abstract Factory implementation.

## How to implement

1. Map out a matrix of distinct product types versus variants of these products.

2. Declare abstract product interface for all product types. Then make all concrete product classes implement these interfaces.

3. Declare the abstract factory interface with a set of creation methods for all abstract products.

4. Implement a set of concrete factory classes, one for each product variant

5. Create factory initialization code somewhere in the app. It should instantiate one of the concrete factory classes, depending on the application configuration or the current environment. Pass this factory object to all classes that construct products.

6. Scan through the code and find all direct calls to product constructors.

## Pros and Cons

**Pros**

- You can be sure that the products you're getting from a factory are compatible with each other.
- You avoid tight coupling between concrete products and client code
- SRP. You can extract the product creation code into one place, making the code easier to support.
- OCP. You can introduce new variants of products without breaking existing client code.

**Cons**

- The code may become more complicated that it should be, since a lot of new interfaces and classes are introduced along with pattern.
