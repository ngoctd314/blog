# Builder Design Pattern

**Builder** is a creational design pattern that lets you construct complex objects step by step. The pattern allows you to produce different types and representations of an object using the same construction code.

**Restrict**

You might make the program too complex by creating a subclass for every possible configuration of an object.

```go
type House struct {}
type HouseWithGarage {}
type HouseWithSwimmingPool {}
type HouseWithFancyStatues {}
type HouseWithGarden {}
```

You constructor with lots of parameters has its downside: not all the parameters are needed at all times.

## Solution

The Builder pattern suggests that you extract the object construction code out of its own class and move it to separate objects called builders.

You can create several different builder classes that implement the same set of building steps, but in a different manner. Then you can use these builders in the construction process to produce different kinds of objects. Different builders execute the same task in various ways.

## Director

You can go further and extract a series of calls to the builder steps you use to construct a product into a separate class called director. The director class defines the order in which to execute the building steps, while the builder provides the implementation for those steps.

The director knows which building steps to execute to get a working product.

In addition, the director class completely hides the details of product construction from the client code. The client only needs to associate a builder with a director, lauch the construction with the director, and get the result from the builder.

## Structure

1. The Builder interface declares product construction steps that are common to all types of builders.

2. Concrete Builders provide different implementations of the construction types. Concrete builders may produce products that don't follow the common interface.

3. Products are resulting objects. Products constructed by different builders don't have to belong to the same class hierarchy or interface.

4. The Director class defines the order in which to call construction steps, so you can create an reuse specific configurations of products.

5. The Client must associate one of the builder objects with the director.