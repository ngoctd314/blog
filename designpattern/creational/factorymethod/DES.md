# Factory method

Factory Method is a creational design pattern that provides an interface for creating objects in a superclass, but allows subclasses to alter the type of objects that will be created.

## Solution

The Factory Method pattern suggests that you replace direct object construction calls (using the new operator) with calls to a special factory method.

```go
type Logistics interface {
    planDelivery()
    createTransport()
}

type RoadLogistics struct {}
func (c *RoadLogistics) createTransport() {}

type SeaLogistics struct {}
func (c *SeaLogistics) createTransport() {}
```

At first glance, this change may look pointless: we just moved the constructor call from one part of the program to another. However, consider this: now you can override the factory method in a subclass and change the class of products deing created by the method.

All product classes implemenet a common interface, you can pass their objects to the client code without breaking it.

The code that uses the factory method doesn't see a difference between actual products returned by various subclasses. The client treats all products as abstract Transport. The client knows that all transport objects are supposed to have the deliver method, but exactly how it works isn't important to the client.

## Structure

1. The Product declares the interface, which is common to all objects that can be produced by the creator and its subclasses.
2. Concrete Products are different implementations of the product interface
3. The creator class defines the factory method that returns new products objects; It's important that the return type of this method matches the product interface. You can declare the factory method as abstract to force all subclasses to implement their own versions of the method.

Note, despite its name, product creation is not the primary responsibility of the creator. Usually, the creator class already has some core business logic related to products. The factory method helps to decouple this logic from the concrete product classes. Here is an analogy: a large software development company can have a training department for programmers. However, the primary function of the company as a whole is still writing code, not producing programers.

4. Concrete Creators override the base factory method so it returns a different type of product. Note that the factory method doesn't have to create new instances all the time. It can also return existing objects from a cache, an object pool, or another resource.

## Pseudocode

This example illustrates how the Factory Method can be used for creating cross-platform UI elements without coupling the client code to concrete UI classes

The base dialog class uses different UI elements to render its window. Under various operating systems, these elements may look a little bit different, but they should still behave consistently. A button in Windows is still a button in Linux.

When the factory method comes into play, you don't need to rewrite the logic of the dialog for each operating system. If we declare a factory method that products buttons inside the base dialog class, we can later create a dialog subclass that returns Windows-styled buttons from the factory method.

For this pattern work, the base dialog class must work with abstract buttons: a base class or an interface that all concrete buttons follow.

## Application

**Use the Factory Method when you don't know beforehand the exact types and dependencies of the objects your code should work this.**
The Factory Method separates product construction code from the code that actually uses the product. Therefore it's easier to extend the production construction code independently from the rest of the code.

For example, to add new product type to the app, you'll only need to create a new creator subclass and override the factory method in it.  

**Use the Factory Method when you want to provide users of your library or framework with a way to extend its internal components**

Inheritance is probably the easiest way to extend the default behavior of a library or framework. But how would the framework recognize that your subclass should be used instead of a standard component?

## Use the Factory Method when you want to save system resources by reusing existing objects instead of rebuilding them each time

## How to implement

1. Make all products follow the same interface. This interface should declare methods that make sence in every product.
2. Add an empty factory method inside the creator class. The return type of the method should match the common product interface.

## Pros and Cons

**Pros**

- You avoid tight coupling between the creator and the concrete products
- SRP. You can move the product creation code into one place in the program, making the code easier to support
- OCP. You can introduce new types of products into the program without breaking existing client code.

## Implement in Golang

```go
```
