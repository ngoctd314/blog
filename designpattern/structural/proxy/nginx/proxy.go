package nginx

// implement server interface
type nginxProxy struct {
	service                iService
	maxAllowedRequestCount int
	rateLimiter            map[string]int
}

func newNginxProxy(service iService, maxAllowedRequestCount int) nginxProxy {
	return nginxProxy{
		service:                service,
		maxAllowedRequestCount: maxAllowedRequestCount,
		rateLimiter:            make(map[string]int),
	}
}

func (n nginxProxy) handleRequest(url, method string) (int, string) {
	if !n.checkRateLimiting(url) {
		return 403, "Not Allowed"
	}

	return n.service.handleRequest(url, method)
}

func (n nginxProxy) checkRateLimiting(url string) bool {
	n.rateLimiter[url]++
	if n.rateLimiter[url] > n.maxAllowedRequestCount {
		return false
	}
	return true
}
