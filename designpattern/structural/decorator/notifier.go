package decorator

type notifier interface {
	send(msg string)
}
