package decorator

import "fmt"

type sms struct {
	notifier notifier
}

func (s *sms) send(msg string) {
	s.notifier.send(msg)
	fmt.Println("send " + msg + " by sms")
}
