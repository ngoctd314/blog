package decorator

import "fmt"

type defaultNotifier struct {
}

func (n *defaultNotifier) send(msg string) {
	fmt.Println("sms notifier " + msg + " is default notifier")
}
