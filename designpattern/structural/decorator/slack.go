package decorator

import "fmt"

type slack struct {
	notifier notifier
}

func (s *slack) send(msg string) {
	s.notifier.send(msg)
	fmt.Println("send " + msg + " by slack")
}
