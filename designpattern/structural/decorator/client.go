package decorator

// Run .
func Run() {
	noti := &defaultNotifier{}

	fb := &facebook{
		notifier: noti,
	}

	slackFb := &slack{
		notifier: fb,
	}

	slackFb.send("TDN")
}
