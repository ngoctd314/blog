package decorator

import "fmt"

type facebook struct {
	notifier notifier
}

func (f *facebook) send(msg string) {
	f.notifier.send(msg)
	fmt.Println("send " + msg + " by facebook")
}
