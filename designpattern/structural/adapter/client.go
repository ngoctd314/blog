package adapter

import "fmt"

type client struct{}

func (c *client) insertLightningConnectorIntoComputer(com computer) {
	fmt.Println("Client inserts Lightning connector into computer.")
	com.insertIntoLightningPort()
}

func Run() {
	client := client{}
	mac := mac{}

	client.insertLightningConnectorIntoComputer(&mac)

	windowsMachine := windows{}
	windowsAdapter := windowsAdapter1(windowsMachine)

	client.insertLightningConnectorIntoComputer(&windowsAdapter)
}
