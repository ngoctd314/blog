package adapter

import "fmt"

// windowsAdapter implement computer interface
// and has a reference to windows
type windowsAdapter struct {
	windowMachine *windows
}

func (w *windowsAdapter) insertIntoLightningPort() {
	fmt.Println("Adapter converts lightning signal to USB")
	w.windowMachine.insertIntoUSBPort()
}

type windowsAdapter1 windows

func (w windowsAdapter1) insertIntoLightningPort() {
	m := windows(w)
	m.insertIntoUSBPort()
}
