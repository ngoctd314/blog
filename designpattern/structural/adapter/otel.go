package adapter

type hp struct {
	name string
}

func (h hp) get() string {
	return h.name
}

type iComputer interface {
	getName() string
}

type hpAdapter hp

func (h hpAdapter) getName() string {
	return hp(h).get()
}
