package composite

import "fmt"

type file struct {
	name string
}

func (f *file) search(keyword string) {
	fmt.Printf("searching for keyword %s in file %s\n", keyword, f.name)
}

func (f *file) getName() string {
	return f.name
}

type folder struct {
	components []component
	name       string
}

func (f *folder) search(keyword string) {
	fmt.Printf("Searching recursively for keyword %s in folder %s\n", keyword, f.name)
	for _, component := range f.components {
		component.search(keyword)
	}
}

func (f *folder) add(c component) {
	f.components = append(f.components, c)
}

type component interface {
	search(string)
}
