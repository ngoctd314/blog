package composite

type phone struct{}

func (p *phone) price() float64 {
	return phonePrice
}

func (p *phone) canBox() bool {
	return true
}
