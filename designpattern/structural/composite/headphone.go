package composite

type headphone struct{}

func (p *headphone) price() float64 {
	return headphonePrice
}

func (p *headphone) canBox() bool {
	return true
}
