package composite

import "testing"

func Test_order(t *testing.T) {
	phone := &phone{}
	headphone := &headphone{}

	// 100 + 1
	phoneBox := &box{
		value: phone.price(),
	}

	// 5 + 1
	headphoneBox := &box{
		value: headphone.price(),
	}

	// 101 + 6 + 1
	phoneAndHeadphoneBox := &box{
		value: phoneBox.price() + headphoneBox.price(),
	}

	order(phoneBox, headphoneBox, phoneAndHeadphoneBox)
}
