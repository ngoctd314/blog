package composite

type iOrder interface {
	price() float64
	canBox() bool
}

type box struct {
	value float64
}

func (b *box) price() float64 {
	return b.value + boxPrice
}

func (b *box) canBox() bool {
	return true
}
