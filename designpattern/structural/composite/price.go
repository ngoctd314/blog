package composite

const (
	boxPrice       float64 = 1
	phonePrice     float64 = 100
	headphonePrice float64 = 5
)
