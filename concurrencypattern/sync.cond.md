# Detailed explain of sync.Cond in go

## What can sync.Cond be used for?

Cond in golang's sync package implements a condition variable that can be multiple readers to wait for public resources. Each cond is associcated with a lock. When modifying a condition or calling the wait method, it must be locked to protect the condition.

The sync.Cond variable is used to coordinate those goroutines that want to share resources. When the state of shared resources changes, it can be used to modify goroutines blocked by mutex.

## sync.Cond usage scenario

One process is receiving data, and other processes must wait for this process to receive data before they can read the correct data.
If you simply use channel or mutex, only one process can wait and read the data. There is no way to notify other processes to read the data.

What about this time?

- A global variable can be used to identify whether the first process has received data. The remaining processes repeatedly check the value of the variable until the data is read
- You can also create multiple channels. Each process is blocked on one channel. The process receiving data will notify one by one after the data is received.

## What are the methods of sync.Cond

```go
func NewCond(l Locker) *Cond

// Broadcast wakes all goroutines waiting on C
//
// wakes up goroutine of all waiting condition variables C without lock protection
func (c *Cond) Broadcast()

// Signal wakes one goroutine waiting on c, if there is any
func (c *Cond) Signal()

// Wait atomically unlocks c.L and suspends execution
// of the calling goroutine. After later resuming execution, Wait locks c.L before returning.
// Unlike in other systems, Wait cannot returning unless awoken by Broadcast or Signal.
//
// Because c.L is not locked when Wait first resumes, the caller typically cannot assume that the condition is true when Wait returns.
// c.L.Lock()
// for !condition() {
//     c.Wait()
// }
// c.L.Unlock()
//
func (c *Cond) Wait()
// Calling wait will automatically release the lock c.L and suspend the goroutine where the caller is located.
// Therefore, the current workload will block where the wait method is called. If other processes call signal or
// broadcast to wake up the process, the wait method ends the blocking, locks c.L again, and continues to execute the code behind the wait.

c.L.Lock()
for !condition() {
    c.Wait()
}
// make use of condition
c.L.Unlock()
```
